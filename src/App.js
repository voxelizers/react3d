import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
  useRouteMatch
} from "react-router-dom";
import threeEntryPoint from './ThreeEntryPoint';
import './App.css';
import Info from './ui/Info'

export default class App extends Component {

  // let { path } = useRouteMatch();
  // let { type, name } = useParams();
  threeRootElement;

  constructor({ match }) {
    super();
    let { path } = match;
    this.path = path;
    this.state = {
      type: match.params.type,
      name: match.params.name,
      //     sampleId: match.params.id
    }
    // let { path } = useRouteMatch();
    //   this.path = path;
    //   console.log(path);
  }

  componentDidMount() {
    threeEntryPoint(this.threeRootElement);
  }

  writeInfos(data) {
    this.setState({ logData: data });
  }

  render() {
    return (
      <div>
        <Switch>
          <Route exact path={this.path}>
            {/* {console.log("debug")} */}
            <Sample type={this.state.type} name={this.state.name} text="No ID provided "></Sample>
          </Route>
          <Route path={`${this.path}/:id`}>
            <Sample type={this.state.type} name={this.state.name} text="Running sample #"></Sample>
          </Route>
        </Switch>
        <div ref={element => this.threeRootElement = element} />
      </div>
    );
  }
}

function Sample(props) {
  // The <Route> that rendered this component has a
  // path of `/topics/:topicId`. The `:topicId` portion
  // of the URL indicates a placeholder that we can
  // get from `useParams()`.
  let { id } = useParams();
  console.log(props.text + id);

  return (
    <Info sampleType={props.type} sampleName={props.name} sampleId={id}></Info>
  );
}