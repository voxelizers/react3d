import * as THREE from 'three';
import * as TOOLS from './Toolbox';
// import {Ammo} from 'three/examples/js/libs/ammo';
import img from './resources/eso_dark.jpg';
import 'three/examples/js/loaders/OBJLoader';
import 'three/examples/js/loaders/MTLLoader';
import 'three/examples/js/utils/SceneUtils.js';

import { Spaceship } from './spaceship';
// import * as OBJLoader from 'three-obj-loader';
// import * as OBJMTLLoader from 'three-objmtll-loader';
// OBJLoader(THREE);
// OBJMTLLoader(THREE);
// import "imports?THREE=three!loaders/BinaryLoader";
// import "imports?THREE=three!loaders/BinaryLoader";


export class AvoidGame {

    scene;
    terrainSize = 256;

    Ammo;
    MISC_TOOLS;

    // Physics variables
    collisionConfiguration;
    dispatcher;
    broadphase;
    solver;
    physicsWorld;
    terrainBody;
    dynamicObjects = [];
    transformAux1;

    shieldActive = false;
    isLoaded = false;
    isInitDone = false;

    selected = 1;

    // Exported context
    context;

    // Imported class
    spaceship;

    constructor(scene) {
        this.scene = scene;
        window.Ammo();
        this.Ammo = window.Ammo;
        this.THREE = THREE;
        this.MISC_TOOLS = new TOOLS.Misc();
    }

    initContext() {
        this.context = {
            scene: this.scene,
            dynamicObjects: this.dynamicObjects,
            physicsWorld: this.physicsWorld
        }
    }

    init() {
        this.initPhysics();
        this.initContext();
        this.scene.add(new THREE.AmbientLight(0xffffff, 0.2));


        var geometry = new THREE.PlaneBufferGeometry(10, 10, 8, 8);
        var material = new THREE.MeshBasicMaterial({ wireframe: true, side: THREE.DoubleSide });
        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(0, 0, 0);
        plane.name = "axis_plane";
        this.scene.add(plane)

        let axisGroup = new THREE.Group();

        var geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(0, 0, 0),
            new THREE.Vector3(5, 0, 0)
        );
        axisGroup.add(new THREE.Line(geometry, new THREE.LineBasicMaterial({ color: 0xff0000 })));

        var geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(0, 0, 0),
            new THREE.Vector3(0, 5, 0)
        );
        axisGroup.add(new THREE.Line(geometry, new THREE.LineBasicMaterial({ color: 0x00ff00 })));

        var geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(0, 0, 0),
            new THREE.Vector3(0, 0, 5)
        );
        axisGroup.add(new THREE.Line(geometry, new THREE.LineBasicMaterial({ color: 0x0000ff })));

        // axisGroup.rotateX(-Math.PI / 2);

        this.scene.add(axisGroup);

        // var spotLight = new THREE.SpotLight(0xffffff, 1);
        // spotLight.position.set(15, 40, 35);
        // spotLight.angle = Math.PI / 4;
        // spotLight.penumbra = 0.05;
        // spotLight.decay = 2;
        // spotLight.distance = 200;
        // spotLight.castShadow = true;
        // spotLight.shadow.mapSize.width = 1024;
        // spotLight.shadow.mapSize.height = 1024;
        // spotLight.shadow.camera.near = 10;
        // spotLight.shadow.camera.far = 200;
        // this.scene.add(spotLight);

        var dirLight = new THREE.DirectionalLight(0xffffff, 0.8);
        dirLight.color.setHSL(0.1, 1, 0.95);
        dirLight.position.set(-1, 1.75, 1);
        dirLight.position.multiplyScalar(300);
        // this.scene.add(dirLight);
        dirLight.castShadow = true;
        dirLight.shadow.mapSize.width = 8192;
        dirLight.shadow.mapSize.height = 8192;
        var d = 500;
        dirLight.shadow.camera.left = -d;
        dirLight.shadow.camera.right = d;
        dirLight.shadow.camera.top = d;
        dirLight.shadow.camera.bottom = -d;
        dirLight.shadow.camera.far = 3500;
        dirLight.shadow.bias = -0.0001;
        var dirLightHeper = new THREE.DirectionalLightHelper(dirLight, 10);
        this.scene.add(dirLightHeper)

        //Heightfield
        this.heightsArray = new Float32Array(this.terrainSize * this.terrainSize);
        this.heightfieldHelper = new TOOLS.HeightfieldHelper(this.terrainSize);
        this.heightfieldHelper.fillHeightArray(this.heightsArray, this.terrainSize);

        var geometry = new THREE.PlaneBufferGeometry(2048, 2048, this.terrainSize - 1, this.terrainSize - 1);
        //geometry.rotateX(-Math.PI / 2);
        //initGeometry(geometry);
        this.updateGeometry(geometry, 0);
        var mat = new THREE.MeshPhongMaterial({ color: 0x808080, dithering: true, wireframe: false });
        var terrainMesh = new THREE.Mesh(geometry, mat);
        terrainMesh.rotateX(-Math.PI / 2);
        terrainMesh.receiveShadow = true;
        // terrainMesh.castShadow = true;
        terrainMesh.name = "heightFieldTerrain";
        this.scene.add(terrainMesh);

        // skydome

        var skyGeo = new THREE.SphereGeometry(1024, 25, 25);
        var loader = new THREE.TextureLoader();
        var matTex = loader.load(img);
        matTex.wrapS = THREE.RepeatWrapping;
        matTex.wrapT = THREE.RepeatWrapping;
        mat = new THREE.MeshPhongMaterial({ dithering: true, wireframe: false, map: matTex });
        var sky = new THREE.Mesh(skyGeo, mat);
        sky.material.side = THREE.BackSide;
        sky.name = "space_sky"
        this.scene.add(sky);

        //sphere object
        var radius = 4, margin = 0.05;
        mat = new THREE.MeshPhongMaterial({ color: 0xff0000, dithering: true, wireframe: true })
        var threeObject = new THREE.Mesh(new THREE.SphereGeometry(radius, 20, 20), mat);
        var shape = new this.Ammo.btSphereShape(radius);
        shape.setMargin(margin);

        threeObject.position.set(-50, 50, 0);
        var mass = 5;
        var localInertia = new this.Ammo.btVector3(0, 0, 0);
        shape.calculateLocalInertia(mass, localInertia);
        var transform = new this.Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new this.Ammo.btVector3(threeObject.position.x, threeObject.position.y, threeObject.position.z));
        var motionState = new this.Ammo.btDefaultMotionState(transform);
        var rbInfo = new this.Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
        var body = new this.Ammo.btRigidBody(rbInfo);
        threeObject.userData.physicsBody = body;
        threeObject.userData.physicsCb = this.animSphere;
        threeObject.receiveShadow = true;
        threeObject.castShadow = true;
        threeObject.name = "sphereObj"
        this.scene.add(threeObject);
        this.dynamicObjects.push(threeObject);
        this.physicsWorld.addRigidBody(body);

        //sphere object 2 (invisible)
        var radius = 4, margin = 0.05;
        mat = new THREE.MeshPhongMaterial({ color: 0xff0000, dithering: true, wireframe: true })
        var threeObject = new THREE.Mesh(new THREE.SphereGeometry(radius, 20, 20), mat);
        threeObject.position.set(0, 80, -100);
        threeObject.name = "sphere2"
        threeObject.visible = false;
        this.scene.add(threeObject);

        //sphere object 3 
        var radius = 4, margin = 0.05;
        mat = new THREE.MeshPhongMaterial({ color: 0x00ff00, dithering: true, wireframe: true })
        var threeObject = new THREE.Mesh(new THREE.SphereGeometry(radius, 20, 20), mat);
        threeObject.name = "sphere3";
        threeObject.visible = false;
        this.scene.add(threeObject);

        //wall object
        var margin = 0.05;
        mat = new THREE.MeshPhongMaterial({ color: 0xff0000, dithering: true, wireframe: true })
        var threeObject = new THREE.Mesh(new THREE.BoxGeometry(10, 10, 1), mat);
        var shape = new this.Ammo.btBoxShape(new this.Ammo.btVector3(10, 10, 1));
        shape.setMargin(margin);

        threeObject.position.set(20, 10, -100);
        var mass = 0;
        var localInertia = new this.Ammo.btVector3(0, 0, 0);
        shape.calculateLocalInertia(mass, localInertia);
        var transform = new this.Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new this.Ammo.btVector3(threeObject.position.x, threeObject.position.y, threeObject.position.z));
        var motionState = new this.Ammo.btDefaultMotionState(transform);
        var rbInfo = new this.Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
        var body = new this.Ammo.btRigidBody(rbInfo);
        threeObject.userData.physicsBody = body;
        threeObject.userData.physicsCb = () => { };
        threeObject.receiveShadow = true;
        threeObject.castShadow = true;
        threeObject.name = "wallObj";
        threeObject.visible = false;
        this.scene.add(threeObject);
        this.dynamicObjects.push(threeObject);
        this.physicsWorld.addRigidBody(body);

        // wall bis
        mat = new THREE.MeshPhongMaterial({ color: 0xff0000, dithering: true, wireframe: false })
        var threeObject = new THREE.Mesh(new THREE.BoxGeometry(10, 10, 1), mat);
        threeObject.receiveShadow = true;
        threeObject.castShadow = true;
        threeObject.position.set(0, 10, -50);
        threeObject.name = "character";
        this.scene.add(threeObject);

        //Create a closed wavey loop
        var curve = new THREE.CatmullRomCurve3([
            new THREE.Vector3(256, 100, 512),
            new THREE.Vector3(512, 100, 0),
            new THREE.Vector3(0, 50, 0),
            new THREE.Vector3(256, 100, 512)
        ]);

        var points = curve.getPoints(15);
        var geometry = new THREE.BufferGeometry().setFromPoints(points);

        var material = new THREE.LineBasicMaterial({ color: 0xff0000 });

        // Create the final object to add to the scene
        var curveObject = new THREE.Line(geometry, material);
        curveObject.name = "spaceshipPath";
        curveObject.visible = false;

        this.scene.add(curveObject);


        // Spaceships
        this.spaceship = new Spaceship(this.context);


        var hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.2);
        hemiLight.color.setHSL(0.6, 1, 0.6);
        hemiLight.groundColor.setHSL(0.095, 1, 0.75);
        hemiLight.position.set(0, 500, 0);
        // this.scene.add(hemiLight);

        //

        var dirLight = new THREE.DirectionalLight(0xffffff, 0.3);
        dirLight.color.setHSL(0.1, 1, 0.3);
        dirLight.position.set(3, 0.3, 0.3);
        dirLight.position.multiplyScalar(20);
        // this.scene.add(dirLight);

        dirLight.castShadow = true;

        var d = 20;
        dirLight.shadow.camera.left = -d;
        dirLight.shadow.camera.right = d;
        dirLight.shadow.camera.top = d;
        dirLight.shadow.camera.bottom = -d;

        dirLight.shadowMapWidth = dirLight.shadowMapHeight = 2048;

        dirLight.shadow.camera.near = 20;
        dirLight.shadow.camera.far = 100;

        dirLight.shadowDarkness = 0.3;

        this.lightPole = new THREE.Object3D();

        var x = this.lightPoleCreator(this.lightPole.clone());
        x.position.set(0, 0, 3);
        this.scene.add(x);

        var x = this.lightPoleCreator(this.lightPole.clone());
        x.position.set(-30, 0, 3);
        this.scene.add(x);

        var x = this.lightPoleCreator(this.lightPole.clone());
        x.position.set(30, 0, 3);
        this.scene.add(x);

        // var loader = THREE.GLTFLoader();

    }

    lightPoleCreator(model) {

        var lightPost = new THREE.Object3D();

        // lightPost.add(model);

        // var helper = new THREE.BoundingBoxHelper(model, 0xff0000);
        // helper.update();
        // var boundingBoxSize = helper.box.max.sub(helper.box.min);

        // Light Pole
        var light = new THREE.SpotLight(0xfff48c, 0.8); //0xFFFFDD //0x44ffaa mystic green
        light.angle = 80 * Math.PI / 180;
        light.exponent = 8; // inner circle light falloff softness (penumbra), 1 = hard, 10 = soft
        light.position.set(0, 4, 0);

        light.target.position.set(0, 0, 0);
        lightPost.add(light.target);

        light.castShadow = true;
        // light.shadowBias = 50;
        light.shadowCameraFov = 80;
        light.shadowCameraNear = 0.5;
        light.shadowCameraFar = light.position.y + 1;
        light.shadowMapWidth = light.shadowMapHeight = 1024;
        light.shadowDarkness = 0.5;

        // var helper = new THREE.CameraHelper( light.shadow.camera );
        // nightcamp.sceneObjects.add( helper );

        lightPost.add(light);

        return lightPost;

    };

    initPhysics() {

        // Physics configuration
        this.transformAux1 = new this.Ammo.btTransform()
        this.collisionConfiguration = new this.Ammo.btDefaultCollisionConfiguration();
        this.dispatcher = new this.Ammo.btCollisionDispatcher(this.collisionConfiguration);
        this.broadphase = new this.Ammo.btDbvtBroadphase();
        this.solver = new this.Ammo.btSequentialImpulseConstraintSolver();
        this.physicsWorld = new this.Ammo.btDiscreteDynamicsWorld(this.dispatcher, this.broadphase, this.solver, this.collisionConfiguration);
        this.physicsWorld.setGravity(new this.Ammo.btVector3(0, -10, 0));

        // Create the terrain body
        var groundShape = this.createPhysicsTerrain();
        var groundTransform = new this.Ammo.btTransform();
        groundTransform.setIdentity();
        // Shifts the terrain, since bullet re-centers it on its bounding box.
        groundTransform.setOrigin(new this.Ammo.btVector3(0, 6, 0));
        var groundMass = 0;
        var groundLocalInertia = new this.Ammo.btVector3(0, 0, 0);
        var groundMotionState = new this.Ammo.btDefaultMotionState(groundTransform);
        var groundBody = new this.Ammo.btRigidBody(new this.Ammo.btRigidBodyConstructionInfo(groundMass, groundMotionState, groundShape, groundLocalInertia));
        this.physicsWorld.addRigidBody(groundBody);

    }

    createPhysicsTerrain() {
        // This parameter is not really used, since we are using PHY_FLOAT height data type and hence it is ignored
        var heightScale = 1;
        // Up axis = 0 for X, 1 for Y, 2 for Z. Normally 1 = Y is used.
        var upAxis = 1;
        // hdt, height data type. "PHY_FLOAT" is used. Possible values are "PHY_FLOAT", "PHY_UCHAR", "PHY_SHORT"
        var hdt = "PHY_FLOAT";
        // Set this to your needs (inverts the triangles)
        var flipQuadEdges = false;
        // Creates height data buffer in Ammo heap
        var ammoHeightData = this.Ammo._malloc(4 * this.terrainSize * this.terrainSize);
        // Copy the javascript height data array to the Ammo one.
        var p = 0;
        var p2 = 0;
        for (var j = 0; j < this.terrainSize; j++) {
            for (var i = 0; i < this.terrainSize; i++) {
                // write 32-bit float data to memory
                this.Ammo.HEAPF32[ammoHeightData + p2 >> 2] = 0; //= heightData[p];
                p++;
                // 4 bytes/float
                p2 += 4;
            }
        }
        // Creates the heightfield physics shape
        var heightFieldShape = new this.Ammo.btHeightfieldTerrainShape(
            this.terrainSize,
            this.terrainSize,
            ammoHeightData,
            heightScale,
            0,
            10,
            upAxis,
            hdt,
            flipQuadEdges
        );
        // Set horizontal scale
        var scaleX = 4; //= terrainWidthExtents / (terrainWidth - 1);
        var scaleZ = 4; // terrainDepthExtents / (terrainDepth - 1);
        heightFieldShape.setLocalScaling(new this.Ammo.btVector3(scaleX, 1, scaleZ));
        heightFieldShape.setMargin(0.05);
        return heightFieldShape;
    }

    updatePhysics(deltaTime) {

        this.physicsWorld.stepSimulation(deltaTime, 10);

        // Update objects
        for (var i = 0, il = this.dynamicObjects.length; i < il; i++) {
            var objThree = this.dynamicObjects[i];
            var objPhys = objThree.userData.physicsBody;
            var dist = objThree.position.clone();
            var radialForce = dist.clone();
            var centralForce = dist.clone();
            radialForce.cross(new THREE.Vector3(0, 1, 0));
            radialForce.normalize();
            // radialForce.multiplyScalar(2);
            centralForce.negate();
            // centralForce.multiplyScalar(2);
            var force = new THREE.Vector3();
            force.addVectors(radialForce, centralForce);
            // objPhys.applyForce(new Ammo.btVector3( 0, 10*(10-objPhys.getWorldTransform().getOrigin().y())*(Math.sin(objPhys.getWorldTransform().getOrigin().x())+1)*5, 0 ), new Ammo.btVector3( 0, 0, 0 ));
            // objPhys.applyForce(new Ammo.btVector3( -Math.sin(0.2*objPhys.getWorldTransform().getOrigin().x())*objPhys.getWorldTransform().getOrigin().x(), 0*(10-objPhys.getWorldTransform().getOrigin().y()), -2*objPhys.getWorldTransform().getOrigin().z() ), new Ammo.btVector3( 0, 0, 0 ));
            // objPhys.applyForce(new Ammo.btVector3(force.x, force.y, force.z));
            // objThree.userData.physicsCb(objPhys);

            var ms = objPhys.getMotionState();
            if (ms) {
                ms.getWorldTransform(this.transformAux1);
                var p = this.transformAux1.getOrigin();
                var q = this.transformAux1.getRotation();
                var dirVect = new this.THREE.Vector3(0, 0, 1);
                var quat = new this.THREE.Quaternion(q.x(), q.y(), q.z(), q.w());
                dirVect.applyQuaternion(quat);
                objThree.position.set(p.x(), p.y(), p.z());
                objThree.quaternion.set(q.x(), q.y(), q.z(), q.w());

            }
        }
    }

    updateGeometry(geometry, time) {
        var vertices = geometry.attributes.position.array;
        var uvs = geometry.attributes.uv.array;

        for (var i = 0, j = 0, k = 0, l = vertices.length; i < l; i++ , j += 3, k += 2) {
            // j + 1 because it is the y component that we modify
            vertices[j + 2] = this.heightsArray[i] * 50 * Math.cos(time);
            var col = i % this.terrainSize;
            var row = Math.floor(i / (this.terrainSize - 1));

            uvs[k + 1] = (1 - col / (this.terrainSize - 1))
            uvs[k] = (row / (this.terrainSize - 1))
        }
        geometry.computeVertexNormals();
    }

    updateTerrain(time) {
        /*if ((time - lastTime) < 1) {
            updCount++;
        } else {
            console.log(updCount)
            updCount = 0;
            lastTime = time;
        }*/
        //simplex = new SimplexNoise();
        var terrainGeom = this.scene.getObjectByName("heightFieldTerrain").geometry;
        terrainGeom.attributes.position.needsUpdate = true;
        //fillNoiseTexture();
        this.updateGeometry(terrainGeom, time);
    }

    animLight(time) {

    }

    animSphere(ctx) {
        // objPhys.applyForce(new window.Ammo.btVector3(0,0,10));
        var transform = new ctx.Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new ctx.Ammo.btVector3(0, 10, 0));
        var sphereObj = ctx.dynamicObjects[0].userData.physicsBody;
        sphereObj.setWorldTransform(transform);
        sphereObj.getMotionState().setWorldTransform(transform);
        // sphereObj.setGravity(new ctx.Ammo.btVector3(0, 0, 0));
        var vel = 150 + Math.random() * 100;
        sphereObj.setLinearVelocity(new ctx.Ammo.btVector3(0, 0, -vel));
        sphereObj.setActivationState(true);
        // sphereObj.applyImpulse(new ctx.Ammo.btVector3(0, 20, 500), new ctx.Ammo.btVector3(0, 0, 0));
        console.log("launch sphere @velocity " + vel);
        setTimeout(ctx.animSphere, 4000, ctx, false);
    }

    animShield(active, ctx) {
        var sphereObj = ctx.dynamicObjects[0];
        var wallObj = ctx.dynamicObjects[1].userData.physicsBody;
        var transform = new ctx.Ammo.btTransform();
        transform.setIdentity();
        var character = ctx.scene.getObjectByName("character");
        if (active) {
            if (sphereObj.position.z > -95) {
                console.log("ok: " + sphereObj.position.z);
                transform.setOrigin(new ctx.Ammo.btVector3(0, 10, -100));
                wallObj.setWorldTransform(transform);
                wallObj.getMotionState().setWorldTransform(transform);
                setTimeout(this.animShield, 70, false, this);
                character.material.wireframe = false;
                character.material.opacity = 1;
            } else {
                console.log("reject: " + sphereObj.position.z);
                character.material.wireframe = false;
                character.material.opacity = 0.2;
            }
        } else {
            transform.setOrigin(new ctx.Ammo.btVector3(20, 10, -100));
            wallObj.setWorldTransform(transform);
            wallObj.getMotionState().setWorldTransform(transform);
            character.material.wireframe = true;
            character.material.opacity = 0.5;
        }
    }

    animSpace(time) {
        var sky = this.scene.getObjectByName("space_sky")
        sky.material.map.offset.x = ((time / 40) % 10) / 10;
        sky.material.map.offset.y = ((time / 40) % 10) / 10
    }



    animSphere3(deltaTime) {
        var sphere2 = this.scene.getObjectByName("sphere2");
        var sphere3 = this.scene.getObjectByName("sphere3");
        var vectDiff = sphere3.position.clone().negate();
        vectDiff.add(sphere2.position);
        sphere3.position.add(vectDiff.multiplyScalar(deltaTime));
    }

    animCam(deltaTime) {
        // var targetPos = this.scene.getObjectByName("sphere2").position;
        // var originPos = this.camera.position;
        // var vectDiff = originPos.clone().negate();
        // vectDiff.add(targetPos);
        // originPos.add(vectDiff.multiplyScalar(deltaTime));

        // this.scene.getObjectByName("sphere3").position.add(vectDiff.multiplyScalar(deltaTime));
        // this.camera.quaternion.slerp(this.scene.getObjectByName("sphere2").quaternion.inverse(), 0.5);
        // this.camera.quaternion.inverse();
        var dirVect = new THREE.Vector3(0, 0, -1);
        var quat = new this.THREE.Quaternion();
        this.camera.getWorldQuaternion(quat);
        this.camera.getWorldDirection(dirVect)
        // dirVector.applyQuaternion( quat );
        this.camera.getWorldDirection(dirVect);
        var dirVect = new THREE.Vector3(0, 0, -1);
        this.camera.translateOnAxis(dirVect, this.camera.userData.speed * deltaTime * 50);
    }


    update(time, deltaTime) {
        this.animLight(time);
        this.animSpace(time);
        this.animCam(deltaTime);
        this.animSphere3(deltaTime)
        //this.updateTerrain(time)
        this.updatePhysics(deltaTime);
        this.spaceship.update(1);
        this.spaceship.update(2);
    }

    handleControls(event, type) {
        if (type == "keydown") {
            switch (event.keyCode) {
                case 13:
                    this.spaceship.attachCamTo(this.selected, this.camera)
                    break;

                case 32:
                    // this.handleCam();
                    this.spaceship.fireLaser(this.scene.getObjectByName("spaceship1").position, this.selected);
                    break;

                case 16:
                    this.animShield(true, this);
                    this.scene.getObjectByName("spaceship1").userData.state = 1; // trigger follow route
                    // var spaceship = this.scene.getObjectByName("spaceship");
                    // spaceship.userData.speed = spaceship.userData.speed ? 0 : 1;
                    // this.scene.getObjectByName("laser").userData.speed = 20;
                    break;

                case 90: this.camera.userData.speed = 1; break;    //Z
                case 83: this.camera.userData.speed = -1; break;    //S

                case 96: this.selected = 0; break;
                case 97: this.selected = 1; break;
                case 98: this.selected = 2; break;
                case 99: this.selected = 3; break;
            }
        } else if (type == "keyup") {
            switch (event.keyCode) {
                case 90:    //Z
                case 83: this.camera.userData.speed = 0;
                    break;    //S
            }
        }
    }

    handleCam(camera) {
        if (camera) {
            this.camera = camera;
            this.camera.userData.speed = 0;
        } else {
            var spaceship = this.scene.getObjectByName("spaceship1");
            var spaceshipBody = spaceship.userData.physicsBody;
            var p = spaceshipBody.getWorldTransform().getOrigin();
            var q = spaceshipBody.getWorldTransform().getRotation();
            var quat = new this.THREE.Quaternion(q.x(), q.y(), q.z(), q.w());
            var dirVect = new this.THREE.Vector3(0, 0, 1);
            dirVect.applyQuaternion(quat);

            var sphere2 = this.scene.getObjectByName("sphere2");
            // sphere2.rotation.setFromQuaternion(quat);
            sphere2.position.set(p.x(), p.y(), p.z() - 10);
            sphere2.translateOnAxis(dirVect, -80);
            // sphere2.visible = true;
            // sphere2.visible = false;
            // this.camera.position.set(sphere2.position.x, sphere2.position.y, sphere2.position.z);
            // this.camera.translateOnAxis(dirVect, 100);
            this.camera.lookAt(new THREE.Vector3(p.x(), p.y(), p.z()));
        }
    }
}