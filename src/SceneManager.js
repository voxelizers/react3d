import * as THREE from 'three/build/three.module';
import { DemoList, TestList } from './Samples';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { FlyControls } from "three/examples/jsm/controls/FlyControls";
import * as Ammo from 'three/examples/js/libs/ammo';
import store from './redux/store';

export default (canvas) => {

    const clock = new THREE.Clock();
    const origin = new THREE.Vector3(0, 0, 0);

    const screenDimensions = {
        width: window.innerWidth,//canvas.width,
        height: window.innerHeight
    }

    const mousePosition = {
        x: 0,
        y: 0
    }

    const scene = buildScene();
    const renderer = buildRender(screenDimensions);
    const camera = buildCamera(screenDimensions);
    const controls = buildControls();
    var physics; buildPhysics();
    var sharedState = store.getState();
    var sampleType = sharedState.DemoSamples.sampleType;
    var sampleName = sharedState.DemoSamples.sampleName;
    const sample = sampleType === "demo" ? new DemoList[sampleName](scene, camera, controls, physics) : new TestList[sampleName](scene, camera, controls, physics);
    // sample.handleCam(camera);
    // initInput();
    // initUiInput();
    // const sceneSubjects = createSceneSubjects(scene);

    function buildScene() {
        const scene = new THREE.Scene();
        //scene.background = new THREE.Color("#000");
        // scene.add(new THREE.AmbientLight(0x808080));
        console.log("Scene created")
        return scene;
    }

    function buildRender({ width, height }) {
        var context = canvas.getContext('webgl2');
        var renderer = new THREE.WebGLRenderer({ canvas: canvas, context: context, antialias: true, alpha: true });
        const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1;
        renderer.setPixelRatio(DPR);
        renderer.setSize(width, height);
        renderer.setClearColor(0x000000);
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        renderer.gammaInput = true;
        renderer.gammaOutput = true;

        return renderer;
    }

    function buildCamera({ width, height }) {
        const aspectRatio = width / height;
        const fieldOfView = 60;
        const nearPlane = 4;
        const farPlane = 100;
        // const camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

        const camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.5, 2000000);
        camera.position.x = 0;
        camera.position.y = 0;
        camera.position.z = 256;
        camera.lookAt(new THREE.Vector3(0, 0, 0));

        return camera;
    }

    function buildControls() {
        return new ControlManager(camera, renderer, canvas);
    }

    function buildPhysics() {
        Ammo().then(function (AmmoLib) {
            physics = AmmoLib;
            // init();
            // animate();
            console.log("Ammo loaded")
        });
    }

    // function initUiInput() {
    //     document.addEventListener("custEvt", (e) => sample.updateOnEvent(e), false);
    // }

    function update() {
        const deltaTime = clock.getDelta();

        const elapsedTime = clock.getElapsedTime();

        sample.update(elapsedTime, deltaTime);

        renderer.render(scene, camera);
    }

    // function updateCameraPositionRelativeToMouse() {
    //     camera.position.x += ((mousePosition.x * 0.01) - camera.position.x) * 0.01;
    //     camera.position.y += (-(mousePosition.y * 0.01) - camera.position.y) * 0.01;
    //     camera.lookAt(origin);
    // }

    function onWindowResize() {
        const { width, height } = canvas;

        screenDimensions.width = width;
        screenDimensions.height = height;

        camera.aspect = width / height;
        camera.updateProjectionMatrix();

        renderer.setSize(width, height);
    }

    function onMouseMove(x, y) {
        mousePosition.x = x;
        mousePosition.y = y;

    }

    return {
        update,
        onWindowResize,
        onMouseMove
    }
}

class ControlManager {
    camera;
    renderer;
    canvas;

    Controls = {
        ORBIT: null,
        FLY: null
    };

    current;

    constructor(camera, renderer, canvas) {
        this.camera = camera;
        this.renderer = renderer;
        this.canvas = canvas;
        this.init();
    }

    init() {
        this.Controls.ORBIT = this.initOrbit();
        this.Controls.FLY = this.initFly();
        this.current = "ORBIT";

        window.addEventListener('keydown', (evt) => {
            this.filterEvent(evt);
            // sample.handleControls(evt, 'keydown');
            store.dispatch({
                type: "COMMAND",
                payload: {
                    key: evt.keyCode,
                    isDown: true
                }
            });
        }, false);
        window.addEventListener('keyup', (evt) => {
            this.filterEvent(evt);
            // sample.handleControls(evt, 'keyup');
            store.dispatch({
                type: "COMMAND",
                payload: {
                    key: evt.keyCode,
                    isDown: false
                }
            });
        }, false);

        this.canvas.addEventListener('mousedown', (e) => {
            this.canvas.onmousemove = function (e) {
                store.dispatch({
                    type: "COMMAND",
                    payload: {
                        btn: e.button,
                        moveX: e.movementX,
                        moveY: e.movementY
                    }
                });
            }
            store.dispatch({
                type: "COMMAND",
                payload: {
                    btn: e.button,
                    isDown: true
                }
            });
        }, false);

        this.canvas.addEventListener('mouseup', (e) => {
            store.dispatch({
                type: "COMMAND", payload: {
                    btn: e.button,
                    isDown: false
                }
            });
            this.canvas.onmousemove = null;
        }, false);
    }

    initOrbit() {
        const orbitCtrl = new OrbitControls(this.camera, this.renderer.domElement);
        orbitCtrl.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        orbitCtrl.dampingFactor = 0.05;
        orbitCtrl.screenSpacePanning = false;
        orbitCtrl.minDistance = 100;
        orbitCtrl.maxDistance = 500;
        orbitCtrl.maxPolarAngle = Math.PI / 2;
        return orbitCtrl;
    }

    initFly() {
        const flyCtrl = new FlyControls(this.camera);
        flyCtrl.movementSpeed = 100;
        flyCtrl.domElement = this.renderer.domElement;
        flyCtrl.rollSpeed = Math.PI / 24;
        flyCtrl.autoForward = false;
        flyCtrl.dragToLook = false;
        return flyCtrl;
    }

    switchControls() {
        Object.keys(this.Controls).forEach((val, idx, arrKeys) => {
            if (val === this.current) {
                this.current = arrKeys[(idx + 1) % (arrKeys.length)];
                console.log("switching control from: " + val + " to " + this.current);
            }
        });
    }

    filterEvent(event) {
        switch (event.keyCode) {
            case 9: {
                // event.stopPropagation();
                event.preventDefault();
                break;
            }
            // case 37:
            // case 38:
            // case 39:
            // case 40: {
            //     // console.log(store.getState().Voxels.raycast.locked);
            //     if (store.getState().Voxels.raycast.locked) {
            //         console.log("intercepted key event "+event.keyCode);
            //         event.stopPropagation();
            //         event.preventDefault();
            //     }
            //     break;
            // }
        }
    }

    update(delta) {
        // this.switchControls();
        this.Controls[this.current].update(delta);
        // this.CONTROLS.ORBIT.update();
        // this.CONTROLS.FLY.movementSpeed = 0.33 * d;
        // this.CONTROLS.FLY.update(delta);
    }
}