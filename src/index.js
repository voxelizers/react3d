import React from 'react';
import { render } from 'react-dom'
import store from './redux/store'

// import registerServiceWorker from './registerServiceWorker';
import { WEBGL } from 'three/examples/jsm/WebGL.js';
import Root from './Root'

if ( WEBGL.isWebGL2Available() === false ) {

	document.body.appendChild( WEBGL.getWebGL2ErrorMessage() );

}

render(<Root store={store} />, document.getElementById('root'));

// registerServiceWorker();

