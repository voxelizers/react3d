import {TerrainSample} from './samples/DemoTerrain';
// import {AvoidGame} from './SampleAvoidGame';
import {CarTester} from './samples/DemoCarTest';
import {VoxelsDemo} from './samples/DemoVoxels';
import {Template} from './samples/Template';
// import {Test} from './demos/Test';
import * as Tests from './samples/boxsplit.test';

// export const SampleList = {

export const DemoList = {
    Terrain: TerrainSample,
    Voxels: VoxelsDemo,
    CarTester: CarTester,
    Template: Template,
    // Test: Test
    // AvoidGame: AvoidGame
}

export const TestList = {
    BoxSplit: Tests.BoxSplitTests
    // AvoidGame: AvoidGame
}