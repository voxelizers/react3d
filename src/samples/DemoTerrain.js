import * as THREE from 'three/build/three.module';
import * as SIM from '../common/SimTools';
import * as TOOLS from '../common/Toolbox';
import { Sample } from '../common/Sample';
import { LandscapePresets, HeigthFuncCatalog } from '../common/TerrainPresets'
import * as MaterialCatalog from '../common/catalogs/MaterialCatalog'
import * as TextureCatalog from '../common/catalogs/TextureCatalog'
import LightPresets from '../common/catalogs/LightPresets'
import { HeightFieldTerrain } from '../common/HeightField'
import Vehicle from '../common/Vehicle';
import { VoxelObjects } from '../common/voxels/VoxelObjects';

// import { SimplexNoise } from 'three/examples/jsm/math/SimplexNoise';

export class TerrainSample extends Sample {

    transformAux1;
    heightData = null;
    ammoHeightData = null;
    // time = 0;
    // objectTimePeriod = 3;
    // timeNextSpawn = time + objectTimePeriod;
    // maxNumObjects = 30;

    rockTerrain;
    vehicle;

    constructor(scene, camera, ammo) {
        super(scene, camera, ammo);
        super.initPhysics();
        this.scene.userData.textures = TextureCatalog;
        this.scene.userData.materials = MaterialCatalog;
        // var controls = new OrbitControls( camera, renderer.domElement );
        // this.onCustomEvent = this.onCustomEvent.bind(this);
        this.initScene();
        this.initPhysics();
    }

    initScene() {

        var ATMSim = new SIM.AtmosphericSimulator();
        // this.scene.background = ATMSim.background;
        this.scene.fog = ATMSim.fog;
        this.scene.add(ATMSim.ambiantLight);
        this.registerForUpdate(ATMSim);

        var axesHelper = new THREE.AxesHelper(5);
        this.scene.add(axesHelper);

        var sunTracker = new TOOLS.CardinalPointsHelper();
        var sunTrackerMesh = sunTracker.render();
        this.scene.add(sunTrackerMesh)
        var sunSim = new SIM.SunSimulator(sunTracker);
        this.scene.add(sunSim.group);
        this.registerForUpdate(sunSim);
        sunSim.setupUI();
        this.scene.add(new THREE.CameraHelper(sunSim.group.getObjectByName("SunLight").shadow.camera));

        var skySim = new SIM.SkySimulator();
        // this.scene.add(skySim);
        // this.registerForUpdate(skySim);

        // Add Sky
        // var skySim2 = new SIM.AtmosphericScatteringSimulator(sunTracker)
        // this.scene.add(skySim2);
        // this.registerForUpdate(skySim2);


        /*** GEOMETRY ***/

        //Water

        var geometry = new THREE.PlaneBufferGeometry(1024, 1024, this.terrainSize - 1, this.terrainSize - 1);
        //geometry.rotateX(-Math.PI / 2);
        // this.heightfieldHelper.updateGeometry(geometry, 0);

        var waterMesh = new THREE.Mesh(geometry, MaterialCatalog.WATER());
        waterMesh.rotateOnWorldAxis(new THREE.Vector3(1, 0, 0), -Math.PI / 2);
        waterMesh.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), -Math.PI / 2);
        waterMesh.position.y = 50;
        waterMesh.receiveShadow = true;
        waterMesh.name = "water";
        // this.scene.add(waterMesh);


        // Sand
        var GROUND_SIZE = 128;
        // var sandGroundMesh = new THREE.Mesh(LandscapePresets.vallon_sand(GROUND_SIZE), MaterialCatalog.sand);
        // sandGroundMesh.rotateOnWorldAxis(new THREE.Vector3(1, 0, 0), -Math.PI / 2);
        // sandGroundMesh.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), -Math.PI / 2);
        // sandGroundMesh.receiveShadow = true;
        // sandGroundMesh.name = "sandGround";
        // this.scene.add(sandGroundMesh);

        // Rock
        GROUND_SIZE = 256;
        var heightFn = (x, y, t) => VoxelObjects.noise4(new THREE.Vector3(x, y, t), 0);
        heightFn = (x,y,t) => VoxelObjects.noise3(x,y,t);
        var heightfieldTerrain = new HeightFieldTerrain(GROUND_SIZE, GROUND_SIZE, 4, heightFn);
        heightfieldTerrain.setupUI();
        heightfieldTerrain.initPhysics(this.Ammo);
        // heightfieldTerrain.setPreset(LandscapePresets.surrounding_mountains);
        // heightfieldTerrain.useHeigthFunction(HeightFunctions.fnNoise)
        heightfieldTerrain.updateGeometryProgressive();
        heightfieldTerrain.createPhysicsShape();
        //wait until canvas is loaded from UI
        setTimeout(() => { heightfieldTerrain.fillCanvasData(document.querySelector("#testcanvas"), 255) }, 1000);
        // var rockGroundMesh = new THREE.Mesh(LandscapePresets.surrounding_mountains(GROUND_SIZE), MaterialCatalog.rock);
        heightfieldTerrain.terrainMesh.material = MaterialCatalog.SAND();
        heightfieldTerrain.terrainMesh.rotateOnWorldAxis(new THREE.Vector3(1, 0, 0), -Math.PI / 2);
        // heightfieldTerrain.terrainMesh.rotateOnWorldAxis(new THREE.Vector3(0, 1, 0), -Math.PI / 2);
        heightfieldTerrain.terrainMesh.receiveShadow = true;
        heightfieldTerrain.terrainMesh.castShadow = true;
        heightfieldTerrain.terrainMesh.name = "rocks";
        this.scene.add(heightfieldTerrain.terrainMesh);
        this.registerForUpdate(heightfieldTerrain.terrainMesh);
        this.rockTerrain = heightfieldTerrain;

        /*** LIGTHS ***/

        this.thunderLight = new THREE.PointLight(0xffffff, 5, 1000);
        this.thunderLight.position.set(5, 500, -30);
        // this.scene.add(this.thunderLight);

        var CentralSpotLight = LightPresets.CentralSpotLight;
        this.scene.add(CentralSpotLight);
        this.registerForUpdate(CentralSpotLight);

        // var SurroundingSpotLight = LightPresets.SurroundingSpotLight;
        // this.scene.add(SurroundingSpotLight);
        // this.scene.add(new THREE.PointLightHelper(SurroundingSpotLight, 1));
        // this.registerForUpdate(SurroundingSpotLight)

        // var sunLight = LightPresets.Sunlight;
        // this.scene.add(sunLight);

        var data = [];

        // setInterval(() => {
        //     // console.log(this.dayTime);
        //     if (this.scene && this.dayTime) {
        //         this.scene.userData.reactAppCb([]);
        //         data.push({ name: Math.round(this.dayTime * 10) / 10, az: sunSim.getObjectByName("SunLight").position.x, elev: sunSim.getObjectByName("SunLight").position.y })
        //         this.scene.userData.reactAppCb(data);
        //     }
        // }, 2000);

        // console.log(this.Ammo);
    }

    initPhysics() {
        // Create the terrain body
        var groundShape = this.rockTerrain.heightFieldShape;
        // groundShape = this.createTerrainShape();
        var groundTransform = new this.Ammo.btTransform();
        groundTransform.setIdentity();
        // Shifts the terrain, since bullet re-centers it on its bounding box.
        groundTransform.setOrigin(new this.Ammo.btVector3(0, (-50 + 50) / 2, 0));
        var groundMass = 0;
        var groundLocalInertia = new this.Ammo.btVector3(0, 0, 0);
        var groundMotionState = new this.Ammo.btDefaultMotionState(groundTransform);
        var groundBody = new this.Ammo.btRigidBody(new this.Ammo.btRigidBodyConstructionInfo(groundMass, groundMotionState, groundShape, groundLocalInertia));
        this.physicsWorld.addRigidBody(groundBody);
        this.transformAux1 = new this.Ammo.btTransform();

        // Sphere
        var radius = 5;
        var threeObject = new THREE.Mesh(new THREE.SphereBufferGeometry(radius, 20, 20), MaterialCatalog.ROCK());
        var shape = new this.Ammo.btSphereShape(radius);
        shape.setMargin(0.05);

        threeObject.position.set(10, 100, 10);
        var mass = 5 * 5;
        var localInertia = new this.Ammo.btVector3(0, 0, 0);
        shape.calculateLocalInertia(mass, localInertia);
        var transform = new this.Ammo.btTransform();
        transform.setIdentity();
        var pos = threeObject.position;
        transform.setOrigin(new this.Ammo.btVector3(pos.x, pos.y, pos.z));
        var motionState = new this.Ammo.btDefaultMotionState(transform);
        var rbInfo = new this.Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
        var body = new this.Ammo.btRigidBody(rbInfo);
        threeObject.userData.physicsBody = body;
        threeObject.receiveShadow = true;
        threeObject.castShadow = true;
        this.scene.add(threeObject);
        this.registerForPhysicsUpdate(threeObject);
        this.physicsWorld.addRigidBody(body);


        this.vehicle = new Vehicle(this.Ammo);
        this.vehicle.createVehicle(this.physicsWorld, this.scene);
        this.scene.add(this.vehicle.vehicleGroup);
    }


    animLight(time) {
        var z = 500 * Math.sin(time / 20);
        this.greenLight.position.set(40, this.heightfieldHelper.getHeight(z, 40) * 50, z);
        // greenLight.intensity = Math.sin(delta*2)+1;
        this.redlight.position.set(500 * Math.sin(time / 20), 10, -30);
        // redlight.intensity = Math.cos(delta*2)+1;
        // whiteLight.position.set(40, 100*(Math.sin(delta/16) + 1), -30);
        this.whiteLight.position.set(500 * Math.cos(time / 4), 15, 500 * Math.sin(time / 4)); //
        this.thunderLight.intensity = 0;
        if (Math.round(time) % 10 == 0 || Math.round(time) % 10 == 1)
            this.thunderLight.intensity = 0.25 + 0.7 * Math.sin(time * 10) + 4.5 * Math.cos(time * 20) * Math.sin(time * 10);

        var t = Math.round(time) > 5 ? 0 : 1
        t = Math.round(time * 2) % 2;
        t = Math.round(time);
    }

    update(time, delta) {
        Sample.prototype.update.call(this, time, delta);
        this.vehicle.sync(delta);
    }
}