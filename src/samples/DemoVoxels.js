import * as THREE from 'three/build/three.module';
import { Sample } from '../common/Sample';
import { VoxelFactory } from '../common/voxels/VoxelFactory'
import { VoxelHelpers } from '../common/voxels/VoxelHelpers'
import { VoxelObjects } from '../common/voxels/VoxelObjects';
import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise";
import * as MaterialCatalog from '../common/catalogs/MaterialCatalog'
import store from '../redux/store'
import { VIS_MODE } from '../redux/reducers';

export class VoxelsDemo extends Sample {

    voxelVolume;

    constructor(scene, camera, controls, physics) {
        super(scene, camera, controls, physics);
        super.initPhysics();
        this.init();
        this.initVoxelHelpers();
        console.log("Launch demo #" + this.sampleId);
        this[this.sampleId]();
    }
    init() {
        this.scene.add(new THREE.AmbientLight(0xffffff));

        var directionalLight = new THREE.DirectionalLight(0xffffff, 1);
        directionalLight.position.set(1, 1, 1).normalize();
        // this.scene.add( directionalLight );
        var pointLight = new THREE.PointLight(0xffffff, 2, 800);
        // particleLight.add( pointLight );
        pointLight.userData.update = (t) => {
            pointLight.position.setX(30 * Math.cos(t));
        }
        this.scene.add(pointLight);
        this.registerForUpdate(pointLight);

        this.scene.add(VoxelFactory.entityMeshGrp);
    }

    initVoxelHelpers() {
        var cubeScale = 4;
        var matrix = new THREE.Matrix4();
        matrix.makeScale(cubeScale, cubeScale, cubeScale);

        var voxelHelpers = new VoxelHelpers(this.voxelVolume, this.camera);

        this.scene.add(voxelHelpers.initHelpers());
        this.scene.add(voxelHelpers.initVoxelHelper());
        // this.scene.add(voxelHelper.initCubePoints());
        this.scene.add(voxelHelpers.initPolygonHelper());

        // this.scene.add(voxelHelpers.entityBoxes().meshGrp);

        var intersectBoxGrp = new THREE.Group();
        this.scene.add(intersectBoxGrp);

        // voxelHelper.dispBoxes(this.voxelVolume.splitBoxes, intersectBoxGrp);// this.voxelVolume.voxelEntities

        this.registerForUpdate(voxelHelpers);
        store.dispatch({ type: "VIS_MODE" , payload: { mode: VIS_MODE.ENTITIES } });
    }

    getMatrix(posx, posy, posz, scale) {
        var matrix = new THREE.Matrix4();
        var matScale = new THREE.Matrix4().makeScale(scale, scale, scale);
        var matTrans = new THREE.Matrix4().makeTranslation(posx, posy, posz);
        matrix.multiplyMatrices(matScale, matTrans);
        return matrix;
    }

    sandbox1() {
        var simplex = new SimplexNoise();

        var matlFn = () => {
            var mat = MaterialCatalog.ShaderExp2();
            mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
            return mat;
        }

        var size = 64; var chunkSize = size / 2; var range = size / chunkSize;
        var dim = (new THREE.Vector3(1, 1, 1)).multiplyScalar(chunkSize);
        var count = 0;

        for (var y = -range / 2; y < range / 2; y++) {
            for (var z = -range / 2; z < range / 2; z++) {
                for (var x = -range / 2; x < range / 2; x++) {
                    if (count < 1||true) {
                        var translatMat = this.getMatrix(x * chunkSize, y * chunkSize, z * chunkSize, 1);
                        // var rangeBox = new THREE.Box3(new THREE.Vector3(-chunkSize/2, -chunkSize/2, -chunkSize/2), new THREE.Vector3(chunkSize/2, chunkSize/2, chunkSize/2));
                        var rangeBox = new THREE.Box3(new THREE.Vector3(0, 0, 0), new THREE.Vector3(chunkSize, chunkSize, chunkSize));
                        rangeBox.applyMatrix4(translatMat);
                        // this.voxelVolume.addEntity(VoxelObjects.noise(rangeBox), matlFn, translatMat);
                        VoxelFactory.createEntity(VoxelObjects.noise(rangeBox, simplex), matlFn, this.getMatrix(0,0,0,4));
                    }
                    count++
                }
            }
        }
        var radius = 4 * 2 * Math.PI; var d = 64;
        translatMat = this.getMatrix(d, d, d, 1);
        VoxelFactory.createEntity(VoxelObjects.sphere(radius, false), matlFn, translatMat);

    }

    sandbox2() {
        var matlFn = () => {
            var mat = MaterialCatalog.ShaderExp2();
            mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
            return mat;
        }
        var radius = 16; var height = 32; var size = 32; var dist = 16; var matrix;
        matrix = this.getMatrix((new THREE.Vector3(0, 0, 0)).multiplyScalar(dist), 1);
        this.voxelVolume.addEntity(VoxelObjects.sphere(radius, true), matlFn, matrix);
        matrix = this.getMatrix((new THREE.Vector3(1, 0, 0)).multiplyScalar(dist), 1);
        this.voxelVolume.addEntity(VoxelObjects.sphere(radius, true), matlFn, matrix);

    }

    // Three Balls
    sandbox3() {
        var size = 64;
        var radius = 4 * 2 * Math.PI; var height = 64; var d = 64;
        var translatMat;

        var matlFn = () => {
            var mat = MaterialCatalog.ShaderExp2();
            mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
            return mat;
        }

        translatMat = this.getMatrix(0, 0, 0, 1);
        this.voxelVolume.addEntity(VoxelObjects.sphere(radius), matlFn, translatMat);
        this.voxelVolume.addEntity(VoxelObjects.sphere(radius * 0.75), matlFn, translatMat);

        translatMat = this.getMatrix(d, 0.3, 0, 1);
        this.voxelVolume.addEntity(VoxelObjects.sphere(0.98 * radius), matlFn, translatMat);

    }

    // columns
    sandbox4() {
        var size = 64;
        // center = new THREE.Vector3(0, 1 / 2, 0).multiplyScalar(size);
        var vSize = new THREE.Vector3(2, 1, 2).multiplyScalar(size);
        // this.voxelVolume.addEntity(VoxelObjects.box(center, vSize.x, vSize.z, vSize.y), MaterialCatalog.ShaderExp2);
        // this.voxelVolume.addEntity(VoxelObjects.sphere(center, size*1.2), MaterialCatalog.ShaderExp2);

        var radius = 4 * 2 * Math.PI; var height = 64; var d = 64;
        var translatMat;

        var matlFn = () => {
            var mat = MaterialCatalog.ShaderExp2();
            mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
            return mat;
        }

        translatMat = this.getMatrix(0, 0, 0, 1);
        this.voxelVolume.addEntity(VoxelObjects.cylinder(radius, height, true), matlFn, translatMat);// INNER FILLED
        this.voxelVolume.addEntity(VoxelObjects.cylinder(radius * 0.75, height, false), matlFn, translatMat);     // INNER EMPTY

        translatMat = this.getMatrix(d, 0.3, 0, 1);
        this.voxelVolume.addEntity(VoxelObjects.cylinder(0.98 * radius, height / 10, true), matlFn, translatMat);
        // translatMat = this.getMatrix((new THREE.Vector3(0, 0.45, 0)).multiplyScalar(dist), 2);
        // this.voxelVolume.addEntity(VoxelObjects.cylinder(0.9 * radius, height / 20, true), matlFn, translatMat);

        // center = (new THREE.Vector3(0, 0.5, 0)).multiplyScalar(dist);

        // this.voxelVolume.addEntity(VoxelObjects.tower(radius, height), MaterialCatalog.ShaderExp2, this.getMatrix(center, 2));
    }

    sandbox() {
        var radius = 6 * 2 * Math.PI;
        var offset = 32;
        var translatMat;

        var matlFn = () => {
            var mat = MaterialCatalog.ShaderExp2();
            mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0));
            return mat;
        }

        // 1 empty big sphere
        translatMat = this.getMatrix(0, 0, 0, 1);
        VoxelFactory.createEntity(VoxelObjects.sphere(radius / 2, 1 / 4, false), matlFn, translatMat);

        // 3 filled spheres around
        translatMat = this.getMatrix(-offset, 0, 0, 1);
        VoxelFactory.createEntity(VoxelObjects.sphere(radius, 1 / 4, true), matlFn, translatMat);
        translatMat = this.getMatrix(0, 0, -offset, 1);
        VoxelFactory.createEntity(VoxelObjects.sphere(radius, 1 / 4, true), matlFn, translatMat);
        translatMat = this.getMatrix(0, -offset, 0, 1);
        // VoxelFactory.createEntity(VoxelObjects.sphere(radius, 1/2, true), matlFn, translatMat);
        var boxMin = new THREE.Vector3(-70, -40, -40);
        var boxMax = new THREE.Vector3(70, 40, 40);
        var rangeBox = new THREE.Box3(boxMin, boxMax);
        VoxelFactory.createRenderer(rangeBox);
    }

}