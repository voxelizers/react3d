import * as THREE from 'three';
import LightPresets from '../common/catalogs/LightPresets'
import * as MaterialCatalog from './catalogs/MaterialCatalog';
import { Sky } from 'three/examples/jsm/objects/Sky'
import store from '../redux/store'

export class SkySimulator {

    skyBox;

    constructor() {
        var skyBoxGeo = new THREE.SphereGeometry(2048, 25, 25);
        this.skyBox = new THREE.Mesh(skyBoxGeo, MaterialCatalog.STARFIELD());
        this.skyBox.name = "space_sky";
        this.skyBox.userData.update = (time) => this.animSpace(time);
        return this.skyBox;

    }

    animSpace(time) {
        // this.skyBox.material.map.offset.x = ((time / 40) % 10) / 10;
        // this.skyBox.material.map.offset.y = ((time / 40) % 10) / 10
    }

}


export class AtmosphericSimulator {
    fog;
    background;
    ambiantLight;
    userData = {};

    fogInterpVals;
    backgroundColorVals = [0x101010, 0x101010, 0x080808, 0x050505,
        0x111111, 0x222222, 0xefd1b5, 0xefd1b5,
        0xcccccc, 0xeeeeee, 0xffffff, 0xffffff,
        0xffffff, 0xffffff, 0xffffff, 0xffffff,
        0xffffff, 0xffffff, 0xefd1b5, 0xefd1b5,
        0xefd1b5, 0x999999, 0x888888, 0x101010]

    constructor() {
        this.userData.update = (time) => this.updateAtmosphere(time)

        var fogInterpCurve = new THREE.SplineCurve([
            new THREE.Vector2(0, 0.0020),
            new THREE.Vector2(5, 0.0025),
            new THREE.Vector2(8, 0.0015),
            new THREE.Vector2(14, 0.0005),
            new THREE.Vector2(20, 0.0015),
            new THREE.Vector2(23, 0.0018),
        ]);

        fogInterpCurve.closed = true;

        this.fogInterpVals = fogInterpCurve.getPoints(24);
        // this.fogInterpVals = [0, 0.0001, 0.0002, 0.0003, 0.0004, 0.0005, 0.0020, 0.0018, 0.0016, 0.0014, 0.0012, 0.0011, 0.001, 0.0009, 0.0008, 0.0005, 0.0005, 0.0009, 0.0012, 0.0015, 0.0020,0.0021,0.0022,0.0023]

        // this.fog = new THREE.FogExp2( 0xefd1b5, 0.0015 );
        this.background = new THREE.Color(0xefd1b5);
        this.ambiantLight = new THREE.AmbientLight(0xffffff, 0.1);
        // return {fog: this.fog, 
        //         background: this.background, 
        //         ambiantLight: this.ambiantLight};

    }

    updateAtmosphere = (time) => {
        var t = ((time - 4) / 4) % 24
        var indice = Math.floor(t);
        // this.fog = new THREE.FogExp2(0xefd1b5, this.fogInterpVals[indice])
        // this.fog = new THREE.FogExp2( 0xefd1b5, 0.0025 );
        // this.scene.fog = new THREE.FogExp2(0xefd1b5, this.fogInterpVals[indice].y)
        // this.fog.density = this.fogInterpVals[indice].y;
        // this.fog.color.setHex(this.backgroundColorVals[indice]);
        this.background.setHex(this.backgroundColorVals[indice]);
        this.ambiantLight.intensity = Math.sin((t) / 24 * Math.PI) * 0.1 + 0.01;
    }
}

export class SunSimulator {
    cardHelper;
    turbidity = 10;
    rayleigh = 2.6;
    mieCoefficient = 0.005;
    mieDirectionalG = 0.995;
    luminance = 1.1;
    inclination = 0.1; // elevation / inclination
    azimuth = 0.25; // Facing front,
    sun = !true;

    sky;
    sunLight;


    constructor(cardinalPointsHelper) {
        this.cardHelper = cardinalPointsHelper;

        // this.SUN_SIM_TABLE.azimuth = sunAziInterpCurve.getPoints( 24 );

        this.sky = new Sky();
        this.sky.scale.setScalar(450000);
        // this.sky.material.wireframe = true;

        this.sunLight = LightPresets.Sunlight;
        // this.sunLight.userData.update = (time) => this.setSunPosition(time);

        this.group = new THREE.Group();
        this.group.add(this.sunLight);
        this.group.add(this.sky);
        this.group.userData.update = (time) => this.update(time);
        this.group.userData.instance = this;

        // return this.group;
    }

    setupUI() {
        store.subscribe(this.onStateChange);
        // store.dispatch({ type: "TIMING", hour: 12, min: 30 });
        store.dispatch({
            type: "UI_SETUP",
            settings: {
                showTimeline: true
            }
        });
    }

    onStateChange = () => {
        var restoredState = store.getState();
        console.log('current time set to: ' + restoredState.DemoSamples.customTime.hour + 'h ' + restoredState.DemoSamples.customTime.min + 'm');
        this.updateDiscrete(restoredState.DemoSamples.customTime.hour);
    }

    update(time) {
        // console.log("setting sun position for time:"+this.SUN_TABLE.time[index])
        var T = (time / 4) % 24
        // this.updateDiscrete(T);
    }

    updateDiscrete(H){
        console.log("setting sun to: "+H+" hour");
        var distFactor = 1024;
        var aziOffset = 360 - 6*360/24;
        this.cardHelper.setCurrentArrowDir((H * 360 / 24 + aziOffset)%360, 0)
        this.sunLight.position.set(this.cardHelper.currDirVect.x * distFactor, this.cardHelper.currDirVect.y * distFactor, this.cardHelper.currDirVect.z * distFactor)
        // this.sunLight.intensity = t>50? Math.sin((t)/24*Math.PI):0;

        // min 0xFFCEA6; max 0xFFEDDE
        // CE(206)   A6(166)
        // DA   BA
        // DE   CA
        // ED   DE
        // 0xCE - 0xED
        // var green = 0x00CE00;
        // var blue = 0x0000A6
        var green = 206 + (237 - 206) * 0;
        var blue = 166 + (222 - 166) * 0;
        if (H > 5 && H < 22) {
            if (Math.abs(H - 14) < 4) {
                green = 206 + (237 - 206) * 1;
                blue = 166 + (222 - 166) * 1;
            }
        } else {

        }
        this.sunLight.color.setRGB(1, green / 255, blue / 255);

        var distance = 4000;

        var uniforms = this.sky.material.uniforms;
        uniforms["turbidity"].value = this.turbidity;
        uniforms["rayleigh"].value = this.rayleigh;
        uniforms["mieCoefficient"].value = this.mieCoefficient;
        uniforms["mieDirectionalG"].value = this.mieDirectionalG;
        uniforms["luminance"].value = this.luminance;

        var theta = Math.PI * (this.inclination - 0.5);
        var phi = this.cardHelper.azimuth - Math.PI;

        // var sunSphere = new THREE.Mesh();
        this.sunLight.intensity = (1-Math.abs(H-14)/5);
        this.sunLight.position.x = distance * Math.cos(phi);
        this.sunLight.position.y = distance * Math.sin(phi) * Math.sin(theta);
        this.sunLight.position.z = distance * Math.sin(phi) * Math.cos(theta);
        // sunSphere.visible = this.sun;
        uniforms["sunPosition"].value.copy(this.sunLight.position);

    }
}