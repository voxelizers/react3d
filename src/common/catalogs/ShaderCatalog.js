import { Vector4, Uniform } from "three/build/three.module";
import * as TextureCatalog from "./TextureCatalog";

var TEST = {

    uniforms: {
        "time": { value: 1.0 },
    },

    vertexShader: [
        "attribute float vertColor;",
        "varying float vc;",
        "uniform float time;",

        "void main() {",
        "vc = vertColor;",

        "gl_Position = projectionMatrix * modelViewMatrix * vec4( position.x, position.y, time, 1.0 );",

        "}"

    ].join("\n"),

    fragmentShader: [
        "in float vc;",

        "void main() {",
        "float alpha = 0.0;",
        "if(vc > 0.4) alpha = vc;",
        "gl_FragColor = vec4( 1.0, 1.0, 0, alpha );",

        "}"

    ].join("\n")

};

var Instance = {

    uniforms: { "layer": { value: 0.0 }, },

    vertexShader: [
        "precision highp float;",
        "attribute vec3 instancePosition;",
        "attribute float flag;",
        "uniform float layer;",
        "",
        "vec3 shiftVertPos(vec3 vertPos, int flag){",
        "   switch(flag){",
        "       case 1:",
        "       if(vertPos.x > 0.0 && vertPos.z > 0.0) { vertPos.x=0.0; vertPos.z=0.0; }",
        "           break;",
        "       case 2:",
        "       if(vertPos.x < 0.0 && vertPos.z > 0.0){ vertPos.x=0.0; vertPos.z=0.0; }",
        "           break;",
        "       case 4:",
        "       if(vertPos.x > 0.0 && vertPos.z < 0.0){ vertPos.x=0.0; vertPos.z=0.0; }",
        "           break;",
        "       case 8:",
        "       if(vertPos.x < 0.0 && vertPos.z<0.0){",
        "           vertPos.x=0.0;",
        "           vertPos.z=0.0;",
        "       }",
        "           break;",
        "       case 3:",
        "       if(vertPos.z>0.0){",
        "           vertPos.z=0.0;",
        "       }",
        "           break;",
        "       case 7:",
        "       if(vertPos.z>0.0 && vertPos.x > 0.0){",
        "           vertPos.x=vertPos.x*0.5;",
        "           vertPos.z=vertPos.z*0.5;",
        "       }",
        "           break;",
        "   }",
        "   return vertPos;",
        "}",
        "",
        "vec4 getCol(int flag){",
        "   vec4 col = vec4(0.5,0.5,0.5,1.0);",
        "   switch(flag){",
        "       case 1:",
        "      if(position.x < 0.0 && position.z<0.0)",
        "           col.g = 1.0;",
        "           break;",
        "       case 2:",
        "      if(position.x > 0.0 && position.z<0.0)",
        "           col.g = 1.0;",
        "           break;",
        "       case 4:",
        "      if(position.x < 0.0 && position.z>0.0)",
        "           col.g = 1.0;",
        "           break;",
        "       case 8:",
        "      if(position.x > 0.0 && position.z>0.0)",
        "           col.g = 1.0;",
        "           break;",
        "       case 3:",
        "      if(position.z<0.0)",
        "           col.b = 1.0;",
        "           break;",
        "       case 7:",
        "      if(position.z<0.0 || position.x < 0.0)",
        "           col.g = 1.0;",
        "           col.b = 1.0;",
        "           break;",
        "       default:",
        "           col = vec4(1.0,1.0,1.0,0.2);",
        "   }",
        "   return col;",
        "}",
        "varying vec4 vColor;",
        "void main(){",
        "	//vColor.rgb = color;",
        "   vColor = getCol(int(flag));",
        "   vColor = vec4(1.0,1.0,1.0,1.0);",
        "if (instancePosition.y != layer)   vColor.a = 0.1;",
        "   vec3 pos = shiftVertPos(position, int(flag));",
        "	gl_Position = projectionMatrix * modelViewMatrix * vec4( position + instancePosition, 1.0 );",
        "}"
    ].join("\n"),

    fragmentShader: [
        "precision highp float;",
        "varying vec4 vColor;",
        "void main() {",
        "	gl_FragColor = vec4( vColor.r,vColor.g,vColor.b, vColor.a );",
        "}"
    ].join("\n")

};

var Color = {

    uniforms: {
    },

    vertexShader: [
        "attribute vec3 color;",
        "varying vec3 col;",
        "void main()",
        "{",
        "   col = color;",
        "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "	gl_Position = projectionMatrix * mvPosition;",
        "}"
    ].join("\n"),

    fragmentShader: [
        "in vec3 col;",
        "void main( void ) {",
        "	gl_FragColor = vec4(col.r,col.g,col.b,1.0);",
        "}"
    ].join("\n")

};

var TriplanarTex = {

    uniforms: {
        "tex": { value: TextureCatalog.rock() }
    },

    vertexShader: [
        "varying vec3 pos;",
        "varying vec3 norm;",
        "void main()",
        "{",
        "   pos = position;",
        "   norm = normal;",
        "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "	gl_Position = projectionMatrix * mvPosition;",
        "}"
    ].join("\n"),

    fragmentShader: [
        "uniform sampler2D tex;",
        "in vec3 pos;",
        "in vec3 norm;",
        "vec4 triplanar_mapping( sampler2D tex, vec3 normal, vec3 position, float scale ) {",
        // Blending factor of triplanar mapping
        "	vec3 bf = normalize( abs( normal ) );",
        "	bf /= dot( bf, vec3( 1.0 ) );",
        // Triplanar mapping
        "	vec2 tx = position.yz * scale;",
        "	vec2 ty = position.zx * scale;",
        "	vec2 tz = position.xy * scale;",
        // Base color
        "	vec4 cx = texture2D(tex, tx) * bf.x;",
        "	vec4 cy = texture2D(tex, ty) * bf.y;",
        "	vec4 cz = texture2D(tex, tz) * bf.z;",
        "	return cx + cy + cz;",
        "}",
        "void main( void ) {",
        "	gl_FragColor = triplanar_mapping( tex, norm, pos, 0.01 );",
        "}"
    ].join("\n")

};


var Experiment = {

    uniforms: {
        "tex": { value: TextureCatalog.rock() }
    },

    vertexShader: [
        "varying vec3 pos;",
        "varying vec3 norm;",
        "void main()",
        "{",
        "   pos = position;",
        "   norm = normal;",
        "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "	gl_Position = projectionMatrix * mvPosition;",
        "}"
    ].join("\n"),

    fragmentShader: [
        "uniform sampler2D tex;",
        "in vec3 pos;",
        "in vec3 norm;",
        "vec4 triplanar_mapping( sampler2D tex, vec3 normal, vec3 position, float scale ) {",
        // Blending factor of triplanar mapping
        "	vec3 bf = normalize( abs( normal ) );",
        "	bf /= dot( bf, vec3( 1.0 ) );",
        // Triplanar mapping
        "	vec2 tx = position.yz * scale;",
        "	vec2 ty = position.zx * scale;",
        "	vec2 tz = position.xy * scale;",
        // Base color
        "	vec4 cx = texture2D(tex, tx) * bf.x;",
        "	vec4 cy = texture2D(tex, ty) * bf.y;",
        "	vec4 cz = texture2D(tex, tz) * bf.z;",
        "	return (cx + cy + cz)*bf.x;",
        "}",
        "void main( void ) {",
        "	gl_FragColor = triplanar_mapping( tex, norm, pos, 0.01 );",
        "}"
    ].join("\n")

};


var Experiment2 = () => {
    return {
        uniforms: (color) => {
            return {
                "tex": { value: TextureCatalog.rock() },
                "custCol": new Uniform(new Vector4(color.r, color.g, color.b, 0))
            }
        },

        vertexShader: [
            "uniform vec4 custCol;",
            "attribute vec3 color;",
            "varying vec4 col;",
            "varying vec3 pos;",
            "varying vec3 norm;",
            "void main()",
            "{",
            "   col = (custCol.r!=0.0 || custCol.g!=0.0 || custCol.b!=0.0)? custCol: vec4(color,custCol.a);",
            "   pos = position;",
            "   norm = normal;",
            "	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
            "	gl_Position = projectionMatrix * mvPosition;",
            "}"
        ].join("\n"),

        fragmentShader: [
            "uniform sampler2D tex;",
            "in vec4 col;",
            "in vec3 pos;",
            "in vec3 norm;",
            "vec4 triplanar_mapping( sampler2D tex, vec3 normal, vec3 position, float scale ) {",
            // Blending factor of triplanar mapping
            "	vec3 bf = normalize( abs( normal ) );",
            "	bf /= dot( bf, vec3( 1.0 ) );",
            // Triplanar mapping
            "	vec2 tx = position.yz * scale;",
            "	vec2 ty = position.zx * scale;",
            "	vec2 tz = position.xy * scale;",
            // Base color
            "	vec4 cx = texture2D(tex, tx) * bf.x;",
            "	vec4 cy = texture2D(tex, ty) * bf.y;",
            "	vec4 cz = texture2D(tex, tz) * bf.z;",
            "	return (cx + cy + cz)*bf.x;",
            "}",
            "void main( void ) {",
            "vec4 triPlanTex = triplanar_mapping( tex, norm, pos, 0.01 );",
            "vec3 blend = triPlanTex.rgb*0.85 + col.rgb*0.15;",
            "	gl_FragColor = vec4(blend,col.a);",
            "}"
        ].join("\n")

    }
};

export { TEST, Instance, TriplanarTex, Color, Experiment, Experiment2 };
