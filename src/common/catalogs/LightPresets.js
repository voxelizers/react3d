import * as THREE from 'three/build/three.module';
var LightPresets = {};
var LightAnimationCatalog = {};

// Spotlight 1
var spotLight = new THREE.SpotLight(0xffffff, 1);
spotLight.name = "CentralSpotLight";
spotLight.position.set(15, 40, 35);
spotLight.angle = Math.PI / 4;
spotLight.penumbra = 0.05;
spotLight.decay = 2;
spotLight.distance = 250;
spotLight.castShadow = true;
spotLight.shadow.mapSize.width = 1024;
spotLight.shadow.mapSize.height = 1024;
spotLight.shadow.camera.near = 10;
spotLight.shadow.camera.far = 200;
spotLight.userData.update = (time) =>LightAnimationCatalog.oscillationXZ(time, spotLight);

LightPresets.CentralSpotLight = spotLight;

// Spotlight 2
var spotLight2 = LightPresets.CentralSpotLight.clone();
spotLight2.name = "SurroundingSpotLight";
spotLight2.position.set(-600,400,0);
spotLight2.angle = Math.PI / 8;
spotLight2.distance = 2000
spotLight2.shadow.camera.far = 15000;
spotLight2.userData.update = (time) => {
    spotLight2.position.z = 80*Math.cos(time/4);
};

LightPresets.SurroundingSpotLight = spotLight2;

LightAnimationCatalog.oscillationXZ = (t, obj) => {
    obj.position.x = 80*Math.cos(t/4);
    obj.position.z = 400*Math.cos(t/10);
}

// Sunlight
const sizeFactor=1;
var dirlight = new THREE.DirectionalLight( 0xFEE465, 1, 100 );
dirlight.name = "SunLight";
dirlight.position.set( 700, 200, 0 ); 			//default; light shining from top
dirlight.castShadow = true;            // default false

//Set up shadow properties for the light
dirlight.shadow.mapSize.width = 1024;  // default
dirlight.shadow.mapSize.height = 1024; // default
dirlight.shadow.camera.left = -500;
dirlight.shadow.camera.right = 500;
dirlight.shadow.camera.bottom = -200;
dirlight.shadow.camera.top = 200;
dirlight.shadow.camera.near = 0.5;    // default
dirlight.shadow.camera.far = 5000;     // default

LightPresets.Sunlight = dirlight;

export default LightPresets;