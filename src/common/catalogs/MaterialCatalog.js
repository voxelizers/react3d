import * as THREE from "three/build/three.module";
import * as TextureCatalog from "./TextureCatalog";
import * as Shaders from './ShaderCatalog';

var WATER = () => {
  return new THREE.MeshStandardMaterial({
    opacity: 0.5,
    transparent: true,
    color: 0x2194ce
  });
};

var SAND = (repeat = 8) => {
  return new THREE.MeshStandardMaterial({
    roughness: 0.8,
    color: 0xffffff,
    side: THREE.DoubleSide,
    metalness: 0.2,
    bumpScale: 0.0005,
    map: TextureCatalog.sand(repeat),
    normalMap: TextureCatalog.sand_norm(repeat)
  });
};

var ROCK = (repeat = 8) => {
  return new THREE.MeshStandardMaterial({
    roughness: 0.8,
    color: 0xffffff,
    metalness: 0.2,
    bumpScale: 0.0005,
    map: TextureCatalog.rock(repeat),
    normalMap: TextureCatalog.rock_norm(repeat)
  });
};

var STARFIELD = (repeat = 8) => {
  return new THREE.MeshPhongMaterial({
    dithering: true,
    wireframe: false,
    map: TextureCatalog.STARFIELD,
    side: THREE.BackSide,
    emissive: 0xffffff,
    emissiveIntensity: 0.5,
    emissiveMap: TextureCatalog.STARFIELD
  });
};

var ShaderTest = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.TEST.uniforms,
    vertexShader: Shaders.TEST.vertexShader,
    fragmentShader: Shaders.TEST.fragmentShader,
    transparent: true,
    wireframe: true
  });
};

var ShaderInst = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.Instance.uniforms,
    vertexShader: Shaders.Instance.vertexShader,
    fragmentShader: Shaders.Instance.fragmentShader,
    transparent: true,
    wireframe: false
  });
};

var ShaderTex = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.TriplanarTex.uniforms,
    vertexShader: Shaders.TriplanarTex.vertexShader,
    fragmentShader: Shaders.TriplanarTex.fragmentShader,
    side: THREE.DoubleSide,
  });
}

var ShaderCol = () => {
  new THREE.ShaderMaterial({
    uniforms: Shaders.Color.uniforms,
    vertexShader: Shaders.Color.vertexShader,
    fragmentShader: Shaders.Color.fragmentShader,
    side: THREE.DoubleSide,
  });
}

var ShaderExp = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.Experiment.uniforms,
    vertexShader: Shaders.Experiment.vertexShader,
    fragmentShader: Shaders.Experiment.fragmentShader,
    side: THREE.DoubleSide,
  });
};

var ShaderExp2 = () => {
  return new THREE.ShaderMaterial({
    uniforms: Shaders.Experiment2().uniforms(0x000000),
    vertexShader: Shaders.Experiment2().vertexShader,
    fragmentShader: Shaders.Experiment2().fragmentShader,
    side: THREE.DoubleSide,
    transparent: true,
    wireframe: false
  });
}

export {
  WATER, ROCK, SAND, STARFIELD,
  ShaderTest, ShaderInst, ShaderTex, ShaderCol, ShaderExp, ShaderExp2
};
