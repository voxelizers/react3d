import * as THREE from 'three/build/three.module';
import img_eso_dark from '../../resources/eso_dark.jpg';
import img_rock from '../../resources/rock.jpg';
import img_rock_norm from '../../resources/rock_norm.png';
import img_rock2 from '../../resources/rock2.jpg';
import img_rock2_norm from '../../resources/rock2_norm.png';
import img_rock2_spec from '../../resources/rock2_spec.png';
import img_sand from '../../resources/ground_sand.jpg';
import img_sand_norm from '../../resources/ground_sand_norm.png';

// TextureCatalog.sinusoid_norm = textureLoader.load("sinusoid_norm.png", (map) => {
//     map.wrapS = THREE.RepeatWrapping;
//     map.wrapT = THREE.RepeatWrapping;
//     map.anisotropy = 4;
//     map.repeat.set(10, 10);
//     return map;
// });

var textureLoader = new THREE.TextureLoader();

var buildTex = (texImg, repeat) => {
    return textureLoader.load(texImg, (map) => {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(repeat, repeat);
        return map;
    });
}

var sand = (repeat) => { return buildTex(img_sand, repeat) };
var sand_norm = (repeat) => { return buildTex(img_sand_norm, repeat) };
var rock = (repeat) => { return buildTex(img_rock, repeat) };
var rock_norm = (repeat) => { return buildTex(img_rock_norm, repeat) };
var rock2 = (repeat) => { return buildTex(img_rock2, repeat) };
var rock2_norm = (repeat) => { return buildTex(img_rock2_norm, repeat) };
var rock2_spec = (repeat) => { return buildTex(img_rock2_spec, repeat) };
var STARFIELD = (repeat) => { return buildTex(img_eso_dark, repeat) };

export {sand, sand_norm, rock, rock_norm, rock2, rock2_norm, rock2_spec, STARFIELD};