import * as THREE from 'three/build/three.module';
import store from '../../redux/store'
import watch from 'redux-watch'
import { VoxelUtils, VoxelFactory } from './VoxelFactory';
import { VoxelObjects } from './VoxelObjects';
import * as MaterialCatalog from '../catalogs/MaterialCatalog'
import { BoxListHelper } from '../misc/Utils'
import { RAYCAST_MODE, VIS_MODE } from '../../redux/reducers';
import { Lut } from 'three/examples/jsm/math/Lut';

/**
 * A set of tools to interact and help visualization of voxel entities
 */
export class VoxelHelpers {
    voxelHelper;
    polygonHlp;
    heightFieldPlane;
    mouse; camera; raycaster;
    oldState;
    selectedEntity;

    constructor(voxelVol, cam) {
        this.VoxelVolume = voxelVol;
        this.camera = cam;
        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();
        // this.vxCubeHelpers = this.VoxelVolume.voxelInstances.map((vxInst) => { return new VoxelCubeHelper(vxInst); });

        this.vxSelectHlpGrp = new THREE.Group();
        document.addEventListener('mousemove', (e) => this.onDocumentMouseMove(e), false);
        // this.store = store;
        this.initStateListeners();
    }

    onRaycastState = (raycastState, oldRaycastState) => {
        var voxelEntity = VoxelFactory.entities[raycastState.entityId];
        var voxel = raycastState.voxelPos ? raycastState.voxelPos.clone() : null;
        if (raycastState.locked != oldRaycastState.locked) {
            console.log(raycastState.locked ? "locked index " + raycastState.index : "unlocked");
            // voxel.applyMatrix4(vxCubeHlp.cubeMatrix);
            var voxelPos = voxel ? voxel : new THREE.Vector3(0, 0, 0); //.applyMatrix4(vxCubeHlp.cubeMatrix)
            this.voxelHelper.visible = true;
            this.voxelHelper.position.set(voxelPos.x, voxelPos.y, voxelPos.z);
            if (!raycastState.locked) this.refresh();
        }
    };

    initStateListeners() {
        VoxelStateListener.CommandStateListener();   
        
        let w = watch(store.getState, 'Voxels.selectedEntity')
        store.subscribe(w((newVal, oldVal, objectPath) => {
            console.log('Selected entity:', newVal);
            this.entityBoxesHlp.singleBox(newVal, true);
        }));
        // w = watch(store.getState, 'Voxels.raycast.entityId')
        // store.subscribe(w((newVal, oldVal, objectPath) => {
        //     // console.log('selected entity: %s', newVal);
        //     // this.selectedEntity = this.VoxelVolume.voxelEntities[store.getState().Voxels.raycast.entityId];
        // }));
        w = watch(store.getState, 'Voxels.raycast');
        store.subscribe(w((newVal, oldVal, objectPath) => {
            this.onRaycastState(newVal, oldVal);
        }));
        w = watch(store.getState, 'Voxels.raycast.locked')
        store.subscribe(w((newVal, oldVal, objectPath) => {
            this.selectedEntity = store.getState().Voxels.raycast.entityId;
            console.log('Locked entity: %s', this.selectedEntity);
        }));
        w = watch(store.getState, 'Voxels.raycast.index')
        store.subscribe(w((newVal, oldVal, objectPath) => {
            if (newVal !== null && newVal !== undefined) {
                var entityId = store.getState().Voxels.raycast.entityId;
                this.highlightPolygon(entityId, 0, newVal);
                this.entityBoxesHlp.singleBox(entityId);
            }
        }));
        w = watch(store.getState, 'Voxels.mode');
        store.subscribe(w((newVal, oldVal, objectPath) => {
            console.log("Vis mode set to: " + VIS_MODE[newVal]);
            this.entityBoxesHlp.allBoxes(false);
            this.mainHlp.visible = true;
            this.overlapBoxesHlp.meshGrp.visible = newVal === VIS_MODE.OVERLAP;
            // this.highlightGroup();
            // this.polygonHlp.visible = (raycastState.mode === 0);
            // this.cubePointsGrp.visible = (raycastState.mode === 1);
            // if (raycastState.locked) store.dispatch({ type: "RAYCAST_TOGGLE_LOCK" });
            // this.onModeChange();
        }));
        w = watch(store.getState, 'Voxels.entities.status')
        store.subscribe(w((newVal, oldVal, objectPath) => {
            var entityBoxes = Object.keys(newVal).map(key => newVal[key][key]);
            if (entityBoxes[0]) {
                this.entityBoxesHlp.refresh(entityBoxes);
                var overlapBoxesArr = Object.keys(newVal).map(key => {
                    var overlapBoxes = Object.keys(newVal[key])
                        .filter(k2 => (k2 !== key) && (newVal[key][k2] !== null))
                        .map(k2 => { return newVal[key][k2]; });
                    // overlapBoxes.splice(key, 1);
                    // overlapBoxes = overlapBoxes.filter(box => box != null);
                    return overlapBoxes;
                });
                this.overlapBoxesHlp.refresh(overlapBoxesArr.flat());
            }
            // console.log(overlapBoxes);
            // console.log(newVal);
            // this.overlapBoxesHlp.refresh(overlapBoxes);
        }));

    }

    initHelpers() {
        this.helpersGrp = new THREE.Group();

        this.mainHlp = new THREE.Group();
        // var outerBoxSize = this.VoxelVolume.volumeSize * this.VoxelVolume.scale; var segmentsNb = outerBoxSize / 16;
        // var geometry = new THREE.BoxGeometry(outerBoxSize, outerBoxSize, outerBoxSize, segmentsNb, segmentsNb, segmentsNb);
        // var material = new THREE.MeshBasicMaterial({ color: 0x050505, wireframe: true });
        // var cube = new THREE.Mesh(geometry, material);
        // this.mainHlp.add(cube);
        this.mainHlp.add(new THREE.GridHelper(128, 10));
        this.mainHlp.add(new THREE.AxesHelper(64));
        this.mainHlp.visible = false;

        this.entityBoxesHlp = new BoxListHelper([], null);
        this.overlapBoxesHlp = new BoxListHelper([], null, (i) => { return 0xff0000; });

        this.helpersGrp.add(this.mainHlp);
        this.helpersGrp.add(this.entityBoxesHlp.meshGrp);
        this.helpersGrp.add(this.overlapBoxesHlp.meshGrp);


        return this.helpersGrp;
    }

    initCubePoints() {
        if (!this.cubePointsGrp) {
            this.cubePointsGrp = new THREE.Group();
            this.vxCubeHelpers.forEach((cubeHelper => {
                this.cubePointsGrp.add(cubeHelper.points());
            }));
        }
        this.cubePointsGrp.visible = false;
        return this.cubePointsGrp;
    }

    initVoxelHelper() {
        this.voxelHelper = new THREE.Mesh(new THREE.BoxBufferGeometry(4, 4, 4),
            new THREE.MeshStandardMaterial({
                opacity: 0.5,
                transparent: true,
                color: 0x0000ff,
                // wireframe: true
            }));
        return this.voxelHelper;
        // this.helpersGrp.add(this.voxelHelper);
    }

    initPolygonHelper() {
        if (!this.polygonHlp) {
            var geometry = new THREE.BufferGeometry();
            geometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(3 * 3), 3));
            var matArr = [];
            matArr.push(new THREE.MeshBasicMaterial({ color: 0xff0000, side: THREE.DoubleSide }));
            // matArr.push( new THREE.MeshBasicMaterial( { color: 0x00ff00 } ));
            this.polygonHlp = new THREE.Mesh(geometry, matArr[0]);
        }
        return this.polygonHlp;
    }

    highlightVoxel(cubeIndex, voxelIndex) {
        // var voxelCoord = vxCubeHlp.VoxelCube.voxels.coords[voxelId];
        // console.log(voxelCoord);
        // var voxelPos = voxelCoord.clone().applyMatrix4(vxCubeHlp.cubeMatrix);
        // store.dispatch({ type: "SET_VOXEL", voxelCoords: voxelPos.toArray() });
        // console.log(voxelCoord);
    }

    highlightPolygon(entIndex, faceIndex) {
        var vxlInst = VoxelFactory.renderers[entIndex];
        var face = vxlInst.getPolygonFace(faceIndex);
        var faceVerts = vxlInst.getFaceVerts(face);
        var posAttr = this.polygonHlp.geometry.attributes.position;
        faceVerts.flat().forEach((v, i) => {
            posAttr.array[i] = v;
        })
        posAttr.needsUpdate = true;
        this.polygonHlp.geometry.computeBoundingSphere();
    }

    highlightGroup(cubeId, grpId) {
        this.vxCubeHelpers.forEach((vxHlp, cubeIndex) => {
            vxHlp.VoxelCube.mesh.material.forEach((mat, grpIndex) => {
                if (cubeId !== undefined && grpId !== undefined) {
                    if (cubeIndex === cubeId && grpIndex === grpId) {
                        mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0))
                        // mat.wireframe = true;
                    } else {
                        mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(1.0, 1.0, 1.0, 0.5))
                        // mat.wireframe = false;
                    }
                } else {
                    mat.uniforms.custCol = new THREE.Uniform(new THREE.Vector4(0.0, 0.0, 0.0, 1.0))
                }
            })
        });
    }

    addObject(pos) {
        var radius = 32; var height = 64;
        var center = new THREE.Vector3(32, 32, 32); //voxelPos.clone();
        VoxelFactory.addEntity(VoxelObjects.column(pos, radius, height), MaterialCatalog.ShaderExp2);
    }

    raycast() {
        var vxState = store.getState().Voxels;
        var raycastIndex;
        var getVoxelFn; var getBoxFn;
        this.raycaster.setFromCamera(this.mouse, this.camera);
        var entityId = Object.keys(VoxelFactory.renderers).find(id => {
            var voxelRenderer = VoxelFactory.renderers[id];
            if (!vxState.raycast.locked && voxelRenderer.mesh) {
                // return (this.pointsRaycast(vcHelper.points));
                var raycastTarget;
                switch (vxState.raycast.mode) {
                    case 0: //polygons
                        raycastTarget = voxelRenderer.mesh;
                        if (!raycastTarget) {
                            console.log("ERROR: no target Obj")
                        }
                        // getVoxelFn = index => vcHelper.VoxelCube.face2vx(index);
                        getVoxelFn = index => voxelRenderer.pos2Vx(index);
                        getBoxFn = index => voxelRenderer.vx2Blk(index);
                        break;
                    case 1: //points
                        // DEPRECATED
                        // raycastTarget = entity.instance.points;
                        // getVoxelFn = index => {
                        //     var ptPosAttr = raycastTarget.geometry.attributes.position;
                        //     var ptPos = new THREE.Vector3(ptPosAttr.getX(index), ptPosAttr.getY(index), ptPosAttr.getZ(index));
                        //     return ptPos;
                        // };
                        break;
                }
                raycastIndex = VoxelUtils.raycasting(this.raycaster, raycastTarget);
                return raycastIndex !== undefined;
            }
        });
        if (raycastIndex) {
            var voxelPos = getVoxelFn(raycastIndex);
            var blkId = getBoxFn ? getBoxFn(raycastIndex) : null;
            store.dispatch({
                type: "RAYCAST_SET", payload: {
                    entityId: entityId,
                    index: raycastIndex,
                    voxelPos: voxelPos
                }
            });
        }
    }

    onVoxelRenderChange(vxlRenderState) {
        if (vxlRenderState.refreshInstances) {
            console.log("INSTANCE REFRESHED");
            this.instancesBoxes();
        }
    }

    refresh() {
    }

    anim(time) {
        // this.animHeightField(time);
        // this.vxCubeHelpers[0].anim(time);
        this.update();
    }

    update() {
        var demoState = store.getState().DemoSamples;
        var vxState = store.getState().Voxels;
        if (vxState.mode === 1)
            this.raycast();
    }

    onDocumentMouseMove = (event) => {
        event.preventDefault();
        this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        this.mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
        // console.log("onmousemouve")
    }

}

class VoxelStateListener {

    static CommandStateListener = () => {
        var w = watch(store.getState, 'DemoSamples.commands.next');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (newVal) {
                var nextEntityId = (store.getState().Voxels.selectedEntity + 1) % VoxelFactory.entityCount;
                store.dispatch({ type: "SELECT_ENTITY", payload: nextEntityId });
            }
        }));
        w = watch(store.getState, 'DemoSamples.commands.prev');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (newVal) {
                var currId = store.getState().Voxels.selectedEntity;
                var nextEntityId = currId > 0 ? currId - 1 : VoxelFactory.entityCount - 1;
                store.dispatch({ type: "SELECT_ENTITY", payload: nextEntityId });
            }
        }));
        w = watch(store.getState, 'DemoSamples.commands.locking');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (store.getState().DemoSamples.commands.locking)
                // if (store.getState().Voxels.raycast.mode === 1)
                store.dispatch({ type: "RAYCAST_TOGGLE_LOCK" });
        }));
        w = watch(store.getState, 'DemoSamples.commands.movex');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            // console.log('%s changed from %s to %s', objectPath, oldVal, newVal);
            // var entity = this.VoxelVolume.voxelEntities[store.getState().Voxels.raycast.entityId];
            // this.VoxelVolume.moveEntity()
        }));
        w = watch(store.getState, 'DemoSamples.commands.left');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (!newVal && store.getState().Voxels.raycast.locked) {
                var translatMat = new THREE.Matrix4().makeTranslation(-4, 0, 0);
                var selectedEntity = store.getState().Voxels.selectedEntity;
                store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: selectedEntity, transform: translatMat } });
            }
            // this.VoxelVolume.renderEntities();
        }));
        w = watch(store.getState, 'DemoSamples.commands.right');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (!newVal && store.getState().Voxels.raycast.locked) {
                var translatMat = new THREE.Matrix4().makeTranslation(4, 0, 0);
                var selectedEntity = store.getState().Voxels.selectedEntity;
                store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: selectedEntity, transform: translatMat } });
            }
            // this.VoxelVolume.renderEntities();
        }));
        w = watch(store.getState, 'DemoSamples.commands.up');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (!newVal && store.getState().Voxels.raycast.locked) {
                var translatMat = new THREE.Matrix4().makeTranslation(0, 0, 4);
                var selectedEntity = store.getState().Voxels.selectedEntity;
                store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: selectedEntity, transform: translatMat } });
            }
            // this.VoxelVolume.renderEntities();
        }));
        w = watch(store.getState, 'DemoSamples.commands.down');
        store.subscribe(w(function (newVal, oldVal, objectPath) {
            if (!newVal && store.getState().Voxels.raycast.locked) {
                var translatMat = new THREE.Matrix4().makeTranslation(0, 0, -4);
                var selectedEntity = store.getState().Voxels.selectedEntity;
                store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: selectedEntity, transform: translatMat } });
            }
            // this.VoxelVolume.renderEntities();
        }));
    }
}