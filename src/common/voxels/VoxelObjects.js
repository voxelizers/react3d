import * as THREE from 'three/build/three.module';

/**
 * VoxelObjects 
 * 
 * A Voxel Object is instanciated trough a VoxelEntity to modify the Global Scalar Field.
 * 
 * The Global Scalar Field has the knowledge of all VoxelEntities/VoxelObjects so that
 * if a query is made on an area where 2 scalar fields are present, 
 * it will reply with a combination of both correspinding to merged voxel scalar values.
 * 

 */
export class VoxelObjects {

    // static simplex = new SimplexNoise();

    static noise = (box, simplex) => {
        return {
            isosurface: (p, t=0) => {
                var p2 = p.clone();//.multiplyScalar(0.25);
                var freq = [0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8];
                var noise = 0;
                for (var i = 0; i < freq.length; i++) {
                    noise += (simplex.noise4d(p2.x * freq[i], p2.z * freq[i], p2.y / 16, t) + 0.5) / Math.pow(2, i + 1);//
                }
                return noise;

            },
            isoLvl: 0.4,
            boundaries: box,
            vxdensity: 0.125
        }
    }

    // BASIC

    static sphere = (radius, vxdensity, isFilled = true, isoLvl = 0.4) => {
        return {
            isosurface: (p, t) => {
                var dist = p.length(); //p.distanceTo(center);
                var voxelSize = 1 / vxdensity;
                // var weigth = dist <= (radius + 2 * voxelSize) ? Math.pow(radius / dist, 2) : 0;
                var val;
    
                if (isFilled) {
                    if (dist <= radius) {
                        val = (radius - dist) * (1 - isoLvl) / radius + isoLvl;
                    } else {
                        val = isoLvl * (1 - (dist - radius) / dist);
                    }
                    // return dist > 0.01 ? radius * isoLvl / dist : 100;
                } else {
                    if (dist <= radius) {
                        val = dist * isoLvl / radius;
                    } else {
                        val = isoLvl + (1 - isoLvl) * (dist - radius) / dist;
                    }
                }
                return val;
            },
            filled: isFilled,
            isoLvl: 0.4,
            boundaries: new THREE.Box3(new THREE.Vector3(- radius, - radius, - radius),
                new THREE.Vector3(radius, radius, radius)),
            vxdensity: vxdensity
        }
    }

    static cylinder = (radius, height, filled = true, isoLvl = 0.4) => {
        return {
            isosurface: (p, t) => {
                var p2 = new THREE.Vector3(p.x, 0, p.z);
                return VoxelObjects.sphere(radius, filled).isosurface(p2);
            },
            isoLvl: isoLvl,
            boundaries: new THREE.Box3(new THREE.Vector3(- radius, - height / 2, - radius),
                new THREE.Vector3(radius, height / 2, radius)),
            vxdensity: 0.5,
            matrix: new THREE.Matrix4()
        }
    }

    static box = (length, width, height) => {
        return {
            isosurface: (vp, t) => {
                // var v = p.clone().add(origin.clone().multiplyScalar(-1));
                return ((vp.x > -length / 2) && (vp.x < length / 2)) &&
                    ((vp.y > -height / 2) && (vp.y < height / 2)) &&
                    ((vp.z > -width / 2) && (vp.z < width / 2))
            },
            isoLvl: 0.4,
            boundaries: new THREE.Box3(new THREE.Vector3(- length / 2, - height / 2, - width / 2),
                new THREE.Vector3(length / 2, height / 2, width / 2)),
            vxdensity: 0.5
        }
    }


    // META OBJECTS (composition of basic objects)

    static tower = (radius, height) => {
        return {
            isosurface: (p, t) => {
                var vals = [];
                // outer
                vals.push(VoxelObjects.cylinder_inner_filled(radius, height).isosurface(p));

                // inner
                vals.push(VoxelObjects.cylinder_inner_empty(radius * 1.5, height).isosurface(p));
                // center = (new THREE.Vector3(-1, 0.8, -1)).multiplyScalar(dist);
                // vals.push(VoxelObjects.cylinder_inner_filled(0.9 * radius, height / 10).isosurface(p));
                // center = (new THREE.Vector3(-1, 0.95, -1)).multiplyScalar(dist);
                var p2 = p.clone();
                p2.y += 0.45;
                vals.push(VoxelObjects.cylinder_inner_filled(0.9 * radius, height / 20).isosurface(p2));
                return Math.min(...vals);
            },
            boundaries: new THREE.Box3(new THREE.Vector3(- radius, - height / 2, - radius),
                new THREE.Vector3(radius, height / 2, radius)),
            vxdensity: 0.5
        }
    }

    // DEPRECATED
    // static noise4 = (p, t) => {
    //     if (!this.simplex) {
    //         console.log("create simplex");
    //         this.simplex = new SimplexNoise();
    //     }
    //     var freq = [0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8];
    //     var noise = 0;
    //     for (var i = 0; i < freq.length; i++) {
    //         noise += (this.simplex.noise4d(p.x * freq[i], p.z * freq[i], p.y, t) + 0.5) / Math.pow(2, i + 1);//
    //     }
    //     return noise;
    //     // return HeightFunctions.noise4d(this.simplex, p.x, p.y, p.z, p.t);
    // }

    // static noise3 = (x, y, t = 0) => {
    //     if (!this.simplex) {
    //         console.log("create simplex");
    //         this.simplex = new SimplexNoise();
    //     }
    //     var freq = [0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8];
    //     var noise = 0;
    //     for (var i = 0; i < freq.length; i++) {
    //         noise += (this.simplex.noise3d(x * freq[i], y * freq[i], t) + 0.5) / Math.pow(2, i + 1);//
    //     }
    //     return noise;
    // }

    // EXPERIMENTS
    // static hall = (p, t, size) => {
    //     return ((p.x > - size) && (p.x < size)) &&
    //         ((p.y > -size) && (p.y < size))
    // }

    // static multicube = (p, t, cubeSize) => {
    //     return (((p.y > 0.3 * cubeSize) && (p.y < 0.59 * cubeSize)) ||
    //         ((p.y > 0.61 * cubeSize) && (p.y < 0.9 * cubeSize))) &&
    //         (((p.x > 0.3 * cubeSize) && (p.x < 0.59 * cubeSize)) ||
    //             ((p.x > 0.61 * cubeSize) && (p.x < 0.9 * cubeSize))) &&
    //         (((p.z > 0.3 * cubeSize) && (p.z < 0.59 * cubeSize)) ||
    //             ((p.z > 0.61 * cubeSize) && (p.z < 0.9 * cubeSize)));;
    // }

    // static stairs = (p, t, cubeSize) => {
    //     return ((p.y < 0.85 * cubeSize) && (p.z < 0.15 * cubeSize)) ||
    //         ((p.y < 0.7 * cubeSize) && (p.z < 0.3 * cubeSize)) ||
    //         ((p.y < 0.55 * cubeSize) && (p.z < 0.45 * cubeSize));;
    // }

    // static pointCloud = (p, t, cubeSize) => {
    //     return ((p.y % 2) < 0.5) && ((p.x % 2) < 0.5) && ((p.z % 2) < 0.5);
    // }

    // static slope = (p, t, cubeSize) => {
    //     return (p.z + p.y - cubeSize) < 0.5;
    // }

}