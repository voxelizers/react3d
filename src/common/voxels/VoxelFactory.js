import * as THREE from 'three/build/three.module';
import store from '../../redux/store'
import { VoxelRenderer } from './VoxelRendering'
import { Lut } from 'three/examples/jsm/math/Lut';
import { BoxSplitter } from '../misc/Utils'
import { VoxelObjects } from './VoxelObjects';
import watch from 'redux-watch'

/** 
 * VoxelEntity
 *
 * A Voxel Entity holds a Voxel Object parametered/instanciated with position (matrix) 
 * and dimension (boxRange) to alter the Global Scalar Field on a specfic volume.
 * 
 * If 2 Voxel Entities collides they will both share a common area of influence (overlapBox) 
 
 * The overlap is responsible for merging voxel objects.
 * 
 */

export class VoxelEntity {

    constructor(entityId, voxelObj, matlFn) {
        this.id = entityId;
        this.voxelDef = voxelObj;
        this.transform = new THREE.Matrix4();
        this.materialFunction = matlFn;
        this.mesh = new THREE.Mesh();
        this.mesh.name = "entity" + this.id;
        // this.voxelRenderers[this.id] = {};
        // this.entityMeshGrp.add(entity.mesh)
        // this.voxelEntities.push(entity);
        // var dim = voxelObj.boundaries.clone().applyMatrix4(matrix);
        store.dispatch({ type: "ADD_ENTITY", payload: { entityId: this.id } });
        // store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: this.id, transform: matrix } });
    }

    initStateListeners() {
        let w = watch(store.getState, 'Voxels.entities.transform.' + this.id)
        store.subscribe(w((newVal, oldVal, objectPath) => {
            console.log(objectPath);
            this.transform = newVal;
            var dimensions = this.voxelDef.boundaries.clone().applyMatrix4(newVal);
            // store.dispatch({ type: "ENTITY_STATUS_UPDATE", payload: { entityId:this.id, dimensions } });
            // var entity = this.voxelEntities[entityId];
            // selector

            var overlapBoxes = this.detectOverlaps(dimensions);
            store.dispatch({ type: "ENTITY_OVERLAP_UPDATE", payload: { entityId: this.id, overlapBoxes } });
        }));
        w = watch(store.getState, 'Voxels.entities.status.' + this.id)
        store.subscribe(w((newVal, oldVal, objectPath) => {
            console.log(objectPath + ": " + newVal + " | " + oldVal);
            // var entity = this.voxelEntities[entityId];
            // this.checkOverlaps(entity);
        }));
    }

    detectOverlaps(entityBox) {
        var entitiesStatus = store.getState().Voxels.entities.status;
        var entitiesDim = Object.keys(entitiesStatus)
            .map(key => entitiesStatus[key][key]);
        entitiesDim[this.id] = entityBox;
        var overlapBoxes = entitiesDim.map((entityBox2, id2) => {
            var overlapBox = entityBox.clone().intersect(entityBox2);
            var unionBox = entityBox.clone().union(entityBox2);
            // var overlap = { entity1: entity.id, entity2: entity2.id, box: overlapBox };
            if (BoxSplitter.checkIntersections(overlapBox)) {
                if (this.id !== id2) {
                    // VoxelFactory.createRenderer(this.id, id2);
                    // store.dispatch({ type: "CREATE_GROUP", payload: { groupId:  } });
                    // console.log(store.getState().Voxels.entities.groups);
                }

                return unionBox;
                // return overlapBox;
            } else {
                return null;
            }
        })
        // entitiesDim.forEach(entityBox => )
        return overlapBoxes;
    }

    getVoxelValue(p, t) {
        var vp = p.clone().applyMatrix4(this.transform.clone().getInverse(this.transform));
        var val = this.voxelDef.isosurface(vp);
        return val;
    }
}
/**
 * VoxelFactory
 * 
 * The voxel factory creates voxel entities & renderers.
 * 
 */

export class VoxelFactory {
    static entityCount = 0;
    static rendererCount = 0;
    static entities = {};
    static renderers = {};
    static overlaps = {};
    static entityMeshGrp = new THREE.Group();

    static createEntity(voxelObj, matlFn, matrix) {
        var entityId = VoxelFactory.entityCount++;
        var voxelEntity = new VoxelEntity(entityId, voxelObj, matlFn, matrix);
        VoxelFactory.entities[entityId] = voxelEntity;
        voxelEntity.initStateListeners();
        // this.createRenderer(entityId);
        store.dispatch({ type: "MOVE_ENTITY", payload: { entityId: entityId, transform: matrix } });
    }

    static createRenderer(rangeBox) {
        var rendererId = VoxelFactory.rendererCount++;
        var entities = VoxelFactory.detectEntities(rangeBox);
        console.log(entities);
        var voxelRenderer = new VoxelRenderer(...entities);
        voxelRenderer.id = rendererId;
        // rangeBox = store.getState().Voxels.entities.status[1][1];
        voxelRenderer.polygonise(rangeBox);
        // voxelRenderer.render();
        // VoxelFactory.renderers[voxelRenderer.id] = voxelRenderer;
    }

    static detectEntities(rangeBox) {
        var entitiesStatus = store.getState().Voxels.entities.status;
        var entities = Object.keys(this.entities).map(function (key) {
            return VoxelFactory.entities[key];
        });

        var intersecting = entities.filter((entity, id) => {
            var entityBox = entitiesStatus[id][id].clone();
            var overlapBox = entityBox.intersect(rangeBox);
            // var unionBox = entityBox.clone().union(entityBox2);
            // var overlap = { entity1: entity.id, entity2: entity2.id, box: overlapBox };
            return BoxSplitter.checkIntersections(overlapBox);
        });
        // entitiesDim.forEach(entityBox => )
        return intersecting;
    }

    static concatIds() {
        // this.id = this.entities
        //     .map(elt => elt.id)
        //     .sort(function (a, b) { return a - b })
        //     .reduce((str, id) => {
        //         return str + '.' + id;
        //     });
        var arr = Array.from(arguments);
        var ids = arr.map(elt => elt.id);
        ids.sort(function (a, b) { return a - b });
        var aggregatedId = ids.reduce((str, id) => {
            return str + '.' + id;
        });
        return aggregatedId;
    }
}


/**
 *      col
row [0-0] [0-1] [0-2]
    [1-0] [1-1] [1-2]
    [2-0] [2-1] [2-2]

entity 0

[0-0]: own box = dimensions
[1-0]: restricted range due to overlap with 1
[2-0]: restricted range due to overlap with 2

rendering area = [0-0] - [1-0] - [2-0]

entity 0 moves => update [0-0] to new range box
    => simple rerender (just move mesh)

check for collisions:
    entity 1 collides with 0
        => update [0-1] with collision (OVERLAP_SIGNAL)
        => rerender with range restriction
    entity 2 collides with 0
        => update [0-2] with collision (OVERLAP_SIGNAL)
        => rerender with range restriction


once each entity has replied with its collision state
refresh entity col:
    col 0 : [0-0] - [1-0] - [2-0]   // range restriction + new range
    => full rerender if col has changed
    
compute new overlap due to collision between 0-1 and 0-2

 */

export class VoxelUtils {

    static raycasting(raycaster, targetObj) {
        var posAttr = targetObj.geometry.attributes.position;
        var colAttr = targetObj.geometry.attributes.color;
        var lastRaycastIndex = store.getState().Voxels.raycast.index;

        var intersects = posAttr ? raycaster.intersectObject(targetObj) : [];

        if (intersects.length > 0) {
            var currIndex;
            if (targetObj instanceof THREE.Mesh)
                currIndex = intersects[0].faceIndex;
            else if (targetObj instanceof THREE.Points)
                currIndex = intersects[0].index;

            if (lastRaycastIndex != currIndex) {
                lastRaycastIndex = currIndex;
                // store.dispatch({
                //     type: "RAYCAST", value: {
                //         index: currIndex,
                //     }
                // });
                return currIndex;
            }
        } else if (lastRaycastIndex !== null) {
            store.dispatch({ type: "RAYCAST_UNSET" });
        }
    }

    polygonsBlocks(blk) {
        var blkNb = this.voxels.groups.length;
        var lut = new Lut("rainbow", blkNb);
        this.voxels.groups.forEach((blk, i) => {
            var col = lut.getColor(i / blkNb);
            var posAttr = blk.attributes.position;
            var indices = [];
            for (var i = 0; i < posAttr.count; i++) {
                var v = new THREE.Vector3(posAttr.getX(i), posAttr.getY(i), posAttr.getZ(i));
                var index = this.voxels.coords.findIndex((v2) => {
                    return v.equals(v2);
                })
                if (index !== -1) indices.push(index);
                else console.log("vertex not found");
            }

            indices.forEach((idx) => {
                var index = idx > 0 ? this.voxels.trianglesSum[idx - 1] : 0;
                var cnt = this.voxels.trianglesCount[idx];
                var arr = this.triangles.ind.slice(index * 3, (index + cnt) * 3);
                arr.forEach((ind) => {
                    this.cubeMesh.geometry.attributes.color.setXYZ(ind, col.r, col.g, col.b);
                })
            })
            this.cubeMesh.geometry.attributes.color.needsUpdate = true;
        })
    }

    makeBlocks(vx) {
        var p = vx.addScalar(0.5);
        var grpIds = [];
        var match = this.voxels.groups.filter((grpGeom, grpId) => {
            if (grpGeom.boundingBox.containsPoint(p)) {
                var posAttr = grpGeom.attributes.position;
                var i = 0;
                while (i < posAttr.updateRange.count) {
                    var v = new THREE.Vector3(posAttr.getX(i), posAttr.getY(i), posAttr.getZ(i));
                    if (p.distanceTo(v) <= 1.5) {
                        grpIds.push(grpId);
                        return grpId;
                        // break;
                    }
                    i++;
                }
            }
        });

        var grpGeom;
        switch (grpIds.length) {
            case 0: // creation
                grpGeom = this.create();
                break;
            case 1: // only check size
                var grpId = grpIds[0];
                grpGeom = this.voxels.groups[grpId];
                var posAttr = grpGeom.attributes.position;
                if (posAttr.updateRange.count >= posAttr.count) {
                    grpGeom = this.resize(posAttr, posAttr.count * 3 * 2);
                }
                break;
            default:    // merge
                grpGeom = this.merge(grpIds)
        }

        var posAttr = grpGeom.attributes.position;
        var vertIndex = posAttr.updateRange.count++;

        posAttr.setXYZ(vertIndex, p.x, p.y, p.z);

        // var boxMin = grpGeom.boundingBox.min; var boxMax = grpGeom.boundingBox.max;
        // var ext = 1.1;
        // boxMin.x = boxMin.x > p.x ? p.x - ext : boxMin.x; boxMin.y = boxMin.y > p.y ? p.y - ext : boxMin.y; boxMin.z = boxMin.z > p.z ? p.z - ext : boxMin.z;
        // boxMax.x = boxMax.x < p.x ? p.x + ext : boxMax.x; boxMax.y = boxMax.y < p.y ? p.y + ext : boxMax.y; boxMax.z = boxMax.z < p.z ? p.z + ext : boxMax.z;

        grpGeom.computeBoundingBox();
        grpGeom.boundingBox.expandByScalar(1.1);
    }

    resize(posAttr, newSize) {
        console.log("resize group");
        var posArr = new Float32Array(newSize * 3);
        posArr.set(posAttr.array.slice(0, posAttr.updateRange.count * 3));
        var grpGeom = new THREE.BufferGeometry();
        grpGeom.addAttribute('position', new THREE.BufferAttribute(posArr, 3));
        grpGeom.attributes.position.updateRange.count = posAttr.updateRange.count;
        // this.points.groups.push(grpGeom);
        return grpGeom;
    }

    create() {
        var grpGeom = new THREE.BufferGeometry();
        grpGeom.addAttribute('position', new THREE.BufferAttribute(new Float32Array(3000), 3));
        grpGeom.attributes.position.updateRange.count = 0;
        this.voxels.groups.push(grpGeom);
        console.log("create group " + (this.voxels.groups.length - 1));
        return grpGeom;
    }

    merge(grpIds) {
        // console.log("merge: " + grpIds);
        var arr = grpIds.reverse().reduce((acc, id, i) => {
            var grp = this.voxels.groups.splice(id, 1)[0];
            var posAttr = grp.attributes.position;
            var count = posAttr.updateRange.count;
            acc = [...acc, ...posAttr.array.slice(0, count * 3)];
            return (acc);
        }, []);
        var posArr = new Float32Array(arr.length * 2);
        posArr.set(arr);
        var grpGeom = new THREE.BufferGeometry();
        grpGeom.addAttribute('position', new THREE.BufferAttribute(posArr, 3));
        grpGeom.attributes.position.updateRange.count = arr.length / 3;
        this.voxels.groups.push(grpGeom);
        return grpGeom;
    }

    finish() {
        this.voxels.groups = this.voxels.groups.map((geom, i) => {
            var posAttr = geom.attributes.position;
            var used = posAttr.updateRange.count;
            var size = posAttr.count;
            // console.log("block: " + used + "/" + size);
            var newGeom = this.resize(posAttr, used);
            newGeom.computeBoundingBox();
            newGeom.boundingBox.expandByScalar(0.5);
            return newGeom;
        });
        var sum = 0;
        this.voxels.trianglesSum = this.voxels.trianglesCount.map((c) => {
            sum += c;
            return sum;
        })
    }
}
