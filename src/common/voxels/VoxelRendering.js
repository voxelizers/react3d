import * as THREE from 'three/build/three.module';
import store from '../../redux/store'
import watch from 'redux-watch'
import { BufferGeometryUtils } from 'three/examples/jsm/utils/BufferGeometryUtils'
import MarchingCubes from './MarchingCube'
import { Lut } from 'three/examples/jsm/math/Lut';
import { VoxelFactory } from './VoxelFactory';

/**
 *  Voxel Entities Renderer
 * 
 * A renderer renders a specific zone of the Global Scalar Field by
 * requesting values to voxel entities belonging to the area.
 * 
 * A renderer holds the geometry of the rendered area.
 * 
 * A renderer can also be configured to follow a single entity ?
 */

export class VoxelRenderer {
    id;
    entities = [];
    renderArea;

    mesh; matrix;
    materialFunction;
    needsRefresh = false;
    // overlapRanges = [];
    // isOverlap = false;

    voxels = {
        coords: [],
        colors: [],
        flags: [],
        steps: [],
        groups: [],
        trianglesCount: [],
        trianglesSum: []
    }

    vertices = {
        position: [],
        color: []
    }

    indicesGroups = [];
    boundingBoxes = [];

    time = 0;

    constructor() {
        var entities = Array.from(arguments);
        this.entities = entities;
        this.vxSpacing = 1 / this.entities[0].voxelDef.vxdensity;
        this.isoLevel = this.entities[0].voxelDef.isoLvl;
        this.materialFunction = this.entities[0].materialFunction;
        this.parentMeshGrp = VoxelFactory.entityMeshGrp;
        // this.type = "Entity" //entities.length > 1? "Overlap":"Entity" ; //entity.id.toString().search("\\.") !== -1;
        this.initStateListeners();
    }
    /**
     * Refresh:
     * a renderer being assigned to an area, each change to an entity 
     * belonging to this area will trigger refresh.
     * If a foreign entity moves or is added, the area could be potentially affected
     * The status matrix, keeps track of all interactions between entities.
     * If one row of the matrix which changed belongs to an entity of the renderer,
     * the renderer will be potentially concerned by the change.
     */

    initStateListeners() {



        // watch for each entity row change
        this.entities.forEach(entity => {
            let w = watch(store.getState, 'Voxels.entities.status.' + entity.id);
            store.subscribe(w((newVal, oldVal, objectPath) => {
                // prevent duplicate refresh
                if (!this.needsRefresh) {
                    this.needsRefresh = true;
                    setTimeout(() => {
                        console.log("Renderer refresh triggered: " + this.needsRefresh);
                        // this.needsRefresh = false;
                        this.polygonise();
                    }, 0);
                }
            }));
        })
    }

    mergeVoxelObjects(voxelObjArr) {
        if (voxelObjArr.length === 1) {
            return voxelObjArr[0];
        }
    }

    mergeGroups(grpIds) {
        // console.log("merge");
        var mergedGrps = grpIds.reduce((grpMerge, id) => {
            var grp = this.indicesGroups[id];
            return [...grpMerge, ...grp]
        }, []);
        var mergedBoxes = grpIds.reduce((boxMerge, id) => {
            var box = this.boundingBoxes[id];
            return boxMerge.union(box)
        }, new THREE.Box3());
        grpIds.reverse().forEach(id => {
            this.indicesGroups.splice(id, 1);
            this.boundingBoxes.splice(id, 1);
        })
        this.indicesGroups.push(mergedGrps);
        this.boundingBoxes.push(mergedBoxes);
    }

    findIndexInGroup(vert, grpId) {
        var index;
        var grp = this.indicesGroups[grpId];
        var i = grp.length - 1;
        while (!index && i >= 0) {
            var ind = grp[i--];
            var vert2 = this.vertices.position[ind];
            if (vert.distanceTo(vert2) < 0.001) {
                index = ind;
            }

        }
        return index;
    }

    addFace = (facePos, color) => {
        // precheck: candidate for match
        var groupIds = [];
        this.boundingBoxes.forEach((box, i) => {
            if (box.distanceToPoint(facePos[0]) < 0.01 || box.distanceToPoint(facePos[1]) < 0.01 || box.distanceToPoint(facePos[2]) < 0.01)
                groupIds.push(i);
        });

        var mask = 0;
        var faceInd = facePos.map((vert, i) => {
            var vertIndex;
            groupIds.forEach((grpId, i) => {
                var index = this.findIndexInGroup(vert, grpId);
                if (index) {
                    vertIndex = index;
                    mask += !(mask & (1 << i)) && 1 << i
                };
            });
            if (!vertIndex) {
                this.vertices.position.push(vert);
                this.vertices.color.push(color);
                vertIndex = this.vertices.position.length - 1;
            }
            return vertIndex;
        });

        groupIds = groupIds.reduce((acc, grpId, i) => {
            return mask & (1 << i) ? [...acc, grpId] : acc;
        }, []);

        var grpId = groupIds.length ? groupIds[0] : null;
        if (groupIds.length === 0) {
            // console.log("create");
            this.boundingBoxes.push(new THREE.Box3());
            this.indicesGroups.push([]);
            grpId = this.indicesGroups.length - 1
        } else if (groupIds.length >= 2) {
            this.mergeGroups(groupIds);
            grpId = this.indicesGroups.length - 1
        }
        this.indicesGroups[grpId].push(...faceInd);
        faceInd.forEach((ind) => this.boundingBoxes[grpId].expandByPoint(this.vertices.position[ind]));
    }

    before() {
        this.vertices = {
            position: [],
            color: []
        }

        this.indicesGroups = [];
        this.boundingBoxes = [];
    }

    finish() {
        var polyCount = this.indicesGroups.reduce((sum, grp) => {
            return sum + grp.length;
        }, 0);
        console.log("Polygons: %s (vertices: %s)", polyCount / 3, this.vertices.position.length);
    }

    getVoxelValue(p, t) {
        var vals = this.entities.map(function (entity) {
            var val = entity.getVoxelValue(p);
            // return val !== null ? val : 0;
            return val;
        });
        var valsFilled = this.entities.filter(function (entity) {
            return entity.voxelDef.filled;
        }).map(function (entity) {
            var val = entity.getVoxelValue(p);
            // return val !== null ? val : 0;
            return val;
        });
        var valsEmpty = this.entities.filter(function (entity) {
            return !entity.voxelDef.filled;
        }).map(function (entity) {
            var val = entity.getVoxelValue(p);
            // return val !== null ? val : 0;
            return val;
        });
        var maxFilled = valsFilled.length ? Math.max(...valsFilled) : 1;
        var minEmpty = valsEmpty.length ? Math.min(...valsEmpty) : 0;
        if (!valsFilled.length)
            return minEmpty;
        else if (!valsEmpty.length)
            return maxFilled;
        else return Math.min(maxFilled, minEmpty);
    }

    getMaterial(index) {
        return this.materialFunction(index);
    }

    polygonise(rangeBox) {
        this.renderArea = rangeBox? rangeBox: this.renderArea;
        var localClock = new THREE.Clock(); localClock.getElapsedTime();
        this.before();
        // var range = this.dimensions; //this.voxelObject.boundaries.clone().applyMatrix4(this.matrix);
        for (var j = this.renderArea.min.y; j < this.renderArea.max.y; j += this.vxSpacing) {
            for (var k = this.renderArea.min.z; k < this.renderArea.max.z; k += this.vxSpacing) {
                // var vertSubList = [], idxSubList = [], colorSubList = [];
                for (var i = this.renderArea.min.x; i < this.renderArea.max.x; i += this.vxSpacing) {
                    // if (this.checkRangeRestriction(new THREE.Vector3(i, j, k))) {
                    // positions
                    var isEdge = ((i === 0) << 1) + ((i === this.voxelCount) << 2) +
                        ((k === 0) << 3) + ((k === this.voxelCount) << 4) +
                        ((j === 0) << 5) + ((j === this.voxelCount) << 6);

                    var boundingPoints = MarchingCubes.getBoundingPoints(i, j, k, this.vxSpacing);
                    var boundingVals = boundingPoints.map(p => this.getVoxelValue(p, this.time));
                    var discriminant = MarchingCubes.getDiscriminant(boundingVals, this.isoLevel);

                    if (discriminant !== 0 && discriminant !== 255) {
                        var voxel = new THREE.Vector3(i, j, k);
                        voxel.addScalar(0.5);
                        var color = [1, 1, 1]; //this.voxelColFn(voxel);
                        this.voxels.coords.push(voxel);
                        // colors
                        // this.colors.push(color.r, color.g, color.b);
                        this.voxels.colors.push(color);
                        this.voxels.flags.push(discriminant);
                        // this.makeBlocks(voxel);

                        var vertices = MarchingCubes.polygonise(discriminant, boundingPoints, boundingVals, this.isoLevel);
                        var faces = [];
                        while (vertices.length)
                            faces.push(vertices.splice(0, 3));
                        this.voxels.trianglesCount.push(faces.length);
                        faces.forEach((face) => this.addFace(face, color));
                    }
                    // }
                }
            }
        }
        // this.finish();
        var polyCount = this.indicesGroups.reduce((sum, grp) => {
            return sum + grp.length;
        }, 0);
        console.log("%s polygons rendered in %s", polyCount / 3, Math.round(localClock.getElapsedTime() * 100) / 100);
        this.needsRefresh = false;
        this.render();
    }

    render() {
        var oldmesh = this.parentMeshGrp.getObjectByName("entity" + this.id);
        this.parentMeshGrp.remove(oldmesh);
        if (this.vertices.position.length) {
            var vertArr = this.vertices.position.map((v) => {
                return v.toArray();
            });
            var lut = new Lut("rainbow", this.indicesGroups.length * 4);
            var matArr = [];
            var geomArr = this.indicesGroups.map((grp, i) => {
                var geom = new THREE.BufferGeometry();
                geom.setIndex(grp);
                geom.addAttribute('position', new THREE.BufferAttribute(new Float32Array(vertArr.flat()), 3));
                geom.addAttribute('color', new THREE.BufferAttribute(new Float32Array(this.vertices.color.flat()), 3));
                geom.computeBoundingSphere();
                matArr.push(this.getMaterial(i));
                return geom;
            });
            var geom = BufferGeometryUtils.mergeBufferGeometries(geomArr, true);
            geom = BufferGeometryUtils.mergeVertices(geom);
            geom.groups.forEach((geomGrp, i) => {
                geomGrp.materialIndex = i;
            })
            geom.computeFaceNormals();
            geom.computeVertexNormals();
            geom.computeBoundingBox();

            var mesh = new THREE.Mesh(geom, matArr);
            mesh.name = "entity" + this.id;
            // mesh.applyMatrix(this.matrix);
            this.parentMeshGrp.add(mesh);
            this.mesh = mesh;
            // mesh.geometry = geom;
            // mesh.material = matArr;
        }
        // this.mesh = mesh;
        // return this.mesh;

    }

    face2Vx(faceIndex) {
        var polygonSum = 0;
        var voxelIndex = this.voxels.trianglesCount.findIndex(polygonCount => {
            polygonSum += polygonCount;
            return (polygonSum > faceIndex && Math.abs(polygonSum - faceIndex) <= polygonCount);
        });
        if (voxelIndex !== -1)
            return this.voxels.coords[voxelIndex].clone();//.applyMatrix4(this.matrix);
        else {
            console.log("undetected voxel")
        }
    }

    pos2Vx(faceIndex) {
        var mesh = this.mesh;
        var faceIndices = [mesh.geometry.index.getX(faceIndex * 3),
        mesh.geometry.index.getY(faceIndex * 3),
        mesh.geometry.index.getZ(faceIndex * 3)];
        var facePosArr = faceIndices.map((vertInd, i) => {
            return new THREE.Vector3(mesh.geometry.attributes.position.getX(vertInd),
                mesh.geometry.attributes.position.getY(vertInd),
                mesh.geometry.attributes.position.getZ(vertInd));
        });
        // find barycentric face position by averaging its 3 vertices
        var avgPos = facePosArr.reduce((acc, curr) => {
            return acc.lerp(curr, 0.5);
        });
        // find closest voxel index
        var voxelIndex; var distMinTreshold = this.vxSpacing + 0.5;
        var minDist = this.voxels.coords.reduce((min, vx, i) => {
            var dist = avgPos.distanceTo(vx);
            if (dist < min) {
                voxelIndex = i;
                return dist
            } else return min;
        }, 10);
        if (minDist <= distMinTreshold) {
            // if (minDist > 1) console.log("dist min: " + minDist);
            return this.voxels.coords[voxelIndex].clone()/*.applyMatrix4(this.matrix)*/;
        }
        else {
            console.log("undetected voxel (min dist too long): " + minDist);
            return new THREE.Vector3(0, 0, 0);
        }
    }

    vx2Blk(faceIndex) {
        var mesh = this.mesh;
        var grps = mesh.geometry.groups;
        var index = faceIndex * 3;
        var grpIdx = grps.findIndex((grp) => {
            return (index >= grp.start) && (index <= (grp.start + grp.count));
        })
        return grpIdx;
    }

    getPolygonFace(faceIndex) {
        var mesh = this.mesh;
        var polyGeom = mesh.geometry;
        return new THREE.Face3(polyGeom.index.getX(faceIndex * 3), polyGeom.index.getY(faceIndex * 3), polyGeom.index.getZ(faceIndex * 3));
    }

    getFaceVerts(face) {
        var mesh = this.mesh;
        var polyPosAttr = mesh.geometry.attributes.position;
        var faceIndexArr = [face.a, face.b, face.c];
        var vertArr = faceIndexArr.map((vertIndex) => {
            var vertex = new THREE.Vector3(polyPosAttr.getX(vertIndex), polyPosAttr.getY(vertIndex), polyPosAttr.getZ(vertIndex));
            // return vertex.applyMatrix4(this.matrix).toArray();
            return vertex.toArray();//.applyMatrix4(this.matrix).toArray();
        });
        return vertArr;
    }

    update() {
        this.polygonise();
        this.render();
    }

    anim(time) {
        // var restoredState = store.getState();
        // var timedelay = restoredState.DemoSamples.time - restoredState.DemoSamples.backupTime;
        // if (timedelay > 25) this.anim(time);
        this.time = time;
    }


}