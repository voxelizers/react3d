import * as THREE from "three";

export class CardinalPointsHelper {
  group;
  northVect = new THREE.Vector3(0, 0, -1);
  currDirVect;
  azimuth;
  elevation;
  northArrow;
  currentArrow;

  constructor() {
    this.northArrow = new THREE.ArrowHelper(
      this.northVect.clone(),
      new THREE.Vector3(0, 0, 0),
      10,
      0x999999
    );
    this.currentArrow = new THREE.ArrowHelper(
      new THREE.Vector3(-1, 0, -1),
      new THREE.Vector3(0, 0, 0),
      10,
      0xffff00
    );
    this.group = new THREE.Group();
    this.group.add(this.northArrow);
    this.group.add(this.currentArrow);
  }

  setCurrentArrowDir(azimAngle, elevAngle) {
    this.azimuth = (azimAngle / 360) * 2 * Math.PI;
    this.elevation = (elevAngle / 360) * 2 * Math.PI;
    this.currDirVect = this.northVect.clone();
    this.currDirVect.applyEuler(
      new THREE.Euler(this.elevation, -this.azimuth, 0, "YXZ")
    );
    this.currentArrow.setDirection(this.currDirVect.normalize());
  }

  render() {
    return this.group;
  }
}

export class Misc {
  Ammo;

  constructor() {
    this.Ammo = window.Ammo;
  }

  createConvexHullPhysicsShape(coords) {
    var shape = new this.Ammo.btConvexHullShape();
    var vec3tmp = new this.Ammo.btVector3(0, 0, 0);
    for (var i = 0, il = coords.length; i < il; i += 3) {
      vec3tmp.setValue(coords[i], coords[i + 1], coords[i + 2]);
      var lastOne = i >= il - 3;
      shape.addPoint(vec3tmp, lastOne);
    }
    return shape;
  }

  convertThreeBulletVect(vectSrc, vectDest) {
    if (vectSrc instanceof THREE.Vector3) {
      if (vectDest) {
        vectDest.setX(vectSrc.x);
        vectDest.setY(vectSrc.y);
        vectDest.setZ(vectSrc.z);
      } else {
        return new this.Ammo.btVector3(vectSrc.x, vectSrc.y, vectSrc.z);
      }
    } else {
      if (vectDest) {
        vectDest.x = vectSrc.x();
        vectDest.y = vectSrc.y();
        vectDest.z = vectSrc.z();
      } else {
        return new THREE.Vector3(vectSrc.x(), vectSrc.y(), vectSrc.z(), 0);
      }
    }
  }
}
