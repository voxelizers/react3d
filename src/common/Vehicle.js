import * as THREE from 'three';
import * as MaterialCatalog from './catalogs/MaterialCatalog';

export default class Vehicle {

    Ammo;
    vehicleGroup;
    vehicle;

    // - Global variables -
    DISABLE_DEACTIVATION = 4;
    TRANSFORM_AUX;

    syncList = [];
    time = 0;
    maxNumObjects = 30;
    // Keybord actions
    actions = {};
    keysActions = {
        "KeyW": 'acceleration',
        "KeyS": 'braking',
        "KeyA": 'left',
        "KeyD": 'right'
    };

    wheelMeshes = [];
    chassisMesh;

    FRONT_LEFT = 0;
    FRONT_RIGHT = 1;
    BACK_LEFT = 2;
    BACK_RIGHT = 3;

    vehicleSteering = 0;

    constructor(ammo) {
        this.Ammo = ammo;
        this.vehicleGroup = new THREE.Group();
        this.TRANSFORM_AUX = new this.Ammo.btTransform();
        window.addEventListener( 'keydown', (e) => this.keydown(e));
        window.addEventListener( 'keyup', (e) => this.keyup(e));
    }

    keyup = (e) => {
        if(this.keysActions[e.code]) {
            this.actions[this.keysActions[e.code]] = false;
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    }
    keydown = (e) => {
        if(this.keysActions[e.code]) {
            this.actions[this.keysActions[e.code]] = true;
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    }

    createWheelMesh(radius, width, scene) {
        var t = new THREE.CylinderGeometry(radius, radius, width, 24, 1);
        t.rotateZ(Math.PI / 2);
        var mesh = new THREE.Mesh(t, MaterialCatalog.ROCK());
        mesh.add(new THREE.Mesh(new THREE.BoxGeometry(width * 1.5, radius * 1.75, radius * .25, 1, 1, 1), MaterialCatalog.ROCK()));
        this.vehicleGroup.add(mesh); //scene.add(mesh);
        return mesh;
    }

    createChassisMesh(w, l, h, scene) {
        var shape = new THREE.BoxGeometry(w, l, h, 1, 1, 1);
        var mesh = new THREE.Mesh(shape, MaterialCatalog.ROCK());
        this.vehicleGroup.add(mesh); //scene.add(mesh);
        return mesh;
    }

    createVehicle(physicsWorld, scene) {
        var pos = new THREE.Vector3(0, 80, 0);
        var quat = new THREE.Quaternion(0, 0, 0, 1);
        var scale = 1.5;

        // Vehicle contants
        var chassisWidth = 1.8*scale;
        var chassisHeight = .6*scale*1.5;
        var chassisLength = 4*scale;
        var massVehicle = 800;
        var wheelAxisPositionBack = -1*scale*1.5;
        var wheelRadiusBack = .4*scale*1.5;
        var wheelWidthBack = .3*scale*2;
        var wheelHalfTrackBack = 1*scale*1.5;
        var wheelAxisHeightBack = .3*scale*0.2;
        var wheelAxisFrontPosition = 1.7*scale;
        var wheelHalfTrackFront = 1*scale*1.5;
        var wheelAxisHeightFront = .3*scale*0.2;
        var wheelRadiusFront = .35*scale*1.5;
        var wheelWidthFront = .2*scale*1.5;
        var friction = 1000;
        var suspensionStiffness = 20.0;
        var suspensionDamping = 2.3*2;
        var suspensionCompression = 4.4;
        var suspensionRestLength = 0.6*scale;
        var rollInfluence = 0.2;
        
        // Chassis
        var geometry = new this.Ammo.btBoxShape(new this.Ammo.btVector3(chassisWidth * .5, chassisHeight * .5, chassisLength * .5));
        var transform = new this.Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new this.Ammo.btVector3(pos.x, pos.y, pos.z));
        transform.setRotation(new this.Ammo.btQuaternion(quat.x, quat.y, quat.z, quat.w));
        var motionState = new this.Ammo.btDefaultMotionState(transform);
        var localInertia = new this.Ammo.btVector3(0, 0, 0);
        geometry.calculateLocalInertia(massVehicle, localInertia);
        var body = new this.Ammo.btRigidBody(new this.Ammo.btRigidBodyConstructionInfo(massVehicle, motionState, geometry, localInertia));
        body.setActivationState(this.DISABLE_DEACTIVATION);
        physicsWorld.addRigidBody(body);
        this.chassisMesh = this.createChassisMesh(chassisWidth, chassisHeight, chassisLength, scene);
        // Raycast Vehicle
        // var engineForce = 0;
        // var vehicleSteering = 0;
        // var breakingForce = 0;
        var tuning = new this.Ammo.btVehicleTuning();
        var rayCaster = new this.Ammo.btDefaultVehicleRaycaster(physicsWorld);
        this.vehicle = new this.Ammo.btRaycastVehicle(tuning, body, rayCaster);
        this.vehicle.setCoordinateSystem(0, 1, 2);
        physicsWorld.addAction(this.vehicle);
        // Wheels
        // var FRONT_LEFT = 0;
        // var FRONT_RIGHT = 1;
        // var BACK_LEFT = 2;
        // var BACK_RIGHT = 3;
        
        var wheelDirectionCS0 = new this.Ammo.btVector3(0, -1, 0);
        var wheelAxleCS = new this.Ammo.btVector3(-1, 0, 0);
        var addWheel = (isFront, pos, radius, width, index) => {
            var wheelInfo = this.vehicle.addWheel(
                pos,
                wheelDirectionCS0,
                wheelAxleCS,
                suspensionRestLength,
                radius,
                tuning,
                isFront);
            wheelInfo.set_m_suspensionStiffness(suspensionStiffness);
            wheelInfo.set_m_wheelsDampingRelaxation(suspensionDamping);
            wheelInfo.set_m_wheelsDampingCompression(suspensionCompression);
            wheelInfo.set_m_frictionSlip(friction);
            wheelInfo.set_m_rollInfluence(rollInfluence);
            var wheelMesh = this.createWheelMesh(radius, width, scene);
            wheelMesh.userData.physicsBody = wheelInfo;
            this.wheelMeshes[index] = wheelMesh
        }
        addWheel(true, new this.Ammo.btVector3(wheelHalfTrackFront, wheelAxisHeightFront, wheelAxisFrontPosition), wheelRadiusFront, wheelWidthFront, this.FRONT_LEFT);
        addWheel(true, new this.Ammo.btVector3(-wheelHalfTrackFront, wheelAxisHeightFront, wheelAxisFrontPosition), wheelRadiusFront, wheelWidthFront, this.FRONT_RIGHT);
        addWheel(false, new this.Ammo.btVector3(-wheelHalfTrackBack, wheelAxisHeightBack, wheelAxisPositionBack), wheelRadiusBack, wheelWidthBack, this.BACK_LEFT);
        addWheel(false, new this.Ammo.btVector3(wheelHalfTrackBack, wheelAxisHeightBack, wheelAxisPositionBack), wheelRadiusBack, wheelWidthBack, this.BACK_RIGHT);

        this.sync(0.1);
        // Sync keybord actions and physics and graphics
        
        this.syncList.push(this.sync);
        return this.vehicleGroup;
    }

    sync(dt) {
        var scale = 2;
        var speed = this.vehicle.getCurrentSpeedKmHour();
        // speedometer.innerHTML = (speed < 0 ? '(R) ' : '') + Math.abs(speed).toFixed(1) + ' km/h';
        var engineForce = 0;
        var breakingForce = 0;
        
        var steeringIncrement = .04;
        var steeringClamp = .5;
        var maxEngineForce = 2000*scale;
        var maxBreakingForce = 100;

        if (this.actions.acceleration) {
            if (speed < -1)
                breakingForce = maxBreakingForce;
            else engineForce = maxEngineForce;
        }
        if (this.actions.braking) {
            if (speed > 1)
                breakingForce = maxBreakingForce;
            else engineForce = -maxEngineForce / 2;
        }
        if (this.actions.left) {
            if (this.vehicleSteering < steeringClamp)
                this.vehicleSteering += steeringIncrement;
            // console.log(vehicleSteering);
        }
        else {
            if (this.actions.right) {
                if (this.vehicleSteering > -steeringClamp)
                    this.vehicleSteering -= steeringIncrement;
            }
            else {
                if (this.vehicleSteering < -steeringIncrement)
                    this.vehicleSteering += steeringIncrement;
                else {
                    if (this.vehicleSteering > steeringIncrement)
                        this.vehicleSteering -= steeringIncrement;
                    else {
                        this.vehicleSteering = 0;
                    }
                }
            }
        }
        this.vehicle.applyEngineForce(engineForce, this.BACK_LEFT);
        this.vehicle.applyEngineForce(engineForce, this.BACK_RIGHT);
        this.vehicle.setBrake(breakingForce / 2, this.FRONT_LEFT);
        this.vehicle.setBrake(breakingForce / 2, this.FRONT_RIGHT);
        this.vehicle.setBrake(breakingForce, this.BACK_LEFT);
        this.vehicle.setBrake(breakingForce, this.BACK_RIGHT);
        this.vehicle.setSteeringValue(this.vehicleSteering, this.FRONT_LEFT);
        this.vehicle.setSteeringValue(this.vehicleSteering, this.FRONT_RIGHT);
        var tm, p, q, i;
        var n = this.vehicle.getNumWheels();
        for (i = 0; i < n; i++) {
            this.vehicle.updateWheelTransform(i, true);
            tm = this.vehicle.getWheelTransformWS(i);
            p = tm.getOrigin();
            q = tm.getRotation();
            // console.log(q.getAngle());
            this.wheelMeshes[i].position.set(p.x(), p.y(), p.z());
            this.wheelMeshes[i].quaternion.set(q.x(), q.y(), q.z(), q.w());
        }
        tm = this.vehicle.getChassisWorldTransform();
        p = tm.getOrigin();
        q = tm.getRotation();
        this.chassisMesh.position.set(p.x(), p.y(), p.z());
        this.chassisMesh.quaternion.set(q.x(), q.y(), q.z(), q.w());
    }

}