import store from '../redux/store'
import watch from 'redux-watch'

export class Sample {

    scene;
    Ammo;
    stateListener;

    // Physics variables
    collisionConfiguration;
    dispatcher;
    broadphase;
    solver;
    physicsWorld;

    dynamicObjects = [];
    arrayObjUpdate = [];
    arrayObjEvent = [];
    dayTime = 0;

    constructor(scene, camera, ctrlMgr, ammo) {
        this.scene = scene;
        this.camera = camera;
        this.ctrlMgr = ctrlMgr;
        // this.ctrlMgr.Controls.ORBIT.enabled = false;
        this.Ammo = ammo;
        this.store = store;
        console.log(store.getState().DemoSamples);
        this.sampleId = store.getState().DemoSamples.sampleId;
        this.initStateListeners(); // this.stateListener = new StateListener();
    }

    initPhysics() {
        // Physics configuration
        this.collisionConfiguration = new this.Ammo.btDefaultCollisionConfiguration();
        this.dispatcher = new this.Ammo.btCollisionDispatcher(this.collisionConfiguration);
        this.broadphase = new this.Ammo.btDbvtBroadphase();
        this.solver = new this.Ammo.btSequentialImpulseConstraintSolver();
        this.physicsWorld = new this.Ammo.btDiscreteDynamicsWorld(this.dispatcher, this.broadphase, this.solver, this.collisionConfiguration);
        this.physicsWorld.setGravity(new this.Ammo.btVector3(0, - 6, 0));
    }

    initStateListeners() {
        let w = watch(store.getState, 'Voxels.raycast.locked')
        store.subscribe(w((newVal, oldVal, objectPath) => {
            this.ctrlMgr.Controls.ORBIT.enableKeys = !newVal;
            // this.selectedEntity = this.VoxelVolume.voxelEntities[store.getState().Voxels.raycast.entityId];
        }));
    }

    registerForUpdate(obj) {
        this.arrayObjUpdate.push(obj)
    }

    unregisterForUpdate(obj) {
        this.arrayObjUpdate.forEach((item, idx, arr) => {
            if (item == obj)
                arr.splice(idx, 1);
        })
    }

    registerForPhysicsUpdate(obj) {
        this.dynamicObjects.push(obj)
    }

    unregisterForPhysicsUpdate(obj) {
        this.dynamicObjects.forEach((item, idx, arr) => {
            if (item == obj)
                arr.splice(idx, 1);
        })
    }


    update(time, delta) {
        // var msTime = Math.round(time*1000)/1000;
        // if(msTime%250<25)  // update approx every 250 ms
        //     this.store.dispatch({ type: "TIMING", subtype: "SET_CURRENT", ms:  msTime});
        this.ctrlMgr.update(delta);// only required if controls.enableDamping = true, or if controls.autoRotate = true
        this.updateItems(time);
        this.updatePhysics(delta);
    }

    updateItems(time) {
        time = time;
        this.dayTime = (time / 4) % 24;
        this.arrayObjUpdate.forEach((item) => {
            if (item.userData)
                item.userData.update(time);
            else if (item.anim)
                item.anim(time);
            else
                item.update(time);
        })
    }

    updatePhysics(deltaTime) {
        this.physicsWorld.stepSimulation(deltaTime, 10);
        // Update objects
        for (var i = 0, il = this.dynamicObjects.length; i < il; i++) {
            var objThree = this.dynamicObjects[i];
            var objPhys = objThree.userData.physicsBody;
            var ms = objPhys.getMotionState();
            if (ms) {
                ms.getWorldTransform(this.transformAux1);
                var p = this.transformAux1.getOrigin();
                var q = this.transformAux1.getRotation();
                objThree.position.set(p.x(), p.y(), p.z());
                objThree.quaternion.set(q.x(), q.y(), q.z(), q.w());
            }
        }
    }
}

// export class StateListener {


//     constructor() {
//         this.init();
//     }

//     init() {
//         let w = watch(store.getState, 'Voxels.raycast.locked')
//         store.subscribe(w((newVal, oldVal, objectPath) => {
//             console.log('selected entity: %s', newVal);
//             // this.selectedEntity = this.VoxelVolume.voxelEntities[store.getState().Voxels.raycast.entityId];
//         }));
//     }
// }