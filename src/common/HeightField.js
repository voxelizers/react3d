import * as THREE from 'three';
import store from '../redux/store'
import MaterialCatalog from './catalogs/MaterialCatalog';
// import { HeightModifiers } from "./heigthfieldPresets";

// This update a geometry according to an heigth function given as parameter
class HeightFieldTerrain {

    Ammo;

    terrainHeigth;
    terrainWidth;
    sizeScale;
    heigthScale = 50;
    heightsArray = [];
    heigthFunc;

    terrainGeom;
    ammoHeightData;
    heightFieldShape;

    constructor(heigth, width, scale, heigthFunc) {
        this.terrainHeigth = heigth;
        this.terrainWidth = width;
        this.sizeScale = scale;
        // this.simplex = new SimplexNoise();
        this.heightsArray = new Float32Array(this.terrainWidth * this.terrainHeigth);
        this.terrainGeom = new THREE.PlaneBufferGeometry(this.terrainWidth * this.sizeScale, this.terrainHeigth * this.sizeScale,
            this.terrainWidth - 1, this.terrainHeigth - 1);
        // this.setHeigthFunc((x,y) => {return 0;});   // default height function return 0
        // this.heigthFunc = (x, y) => { return 0; };
        this.heigthFunc = heigthFunc;
        // this.setUpdBatchNb(1);
        this.terrainGeom.userData = {
            batchCount: 1,
            currentBatch: 0
        };
        this.updateGeometryProgressive(0);
        this.terrainMesh = new THREE.Mesh(this.terrainGeom);
        this.terrainMesh.userData.update = (time) => this.update(time);
        // this.simplex = new SimplexNoise();
    }

    setupUI() {
        // store.subscribe(this.onStateChange);
        // store.dispatch({ type: "TIMING", hour: 12, min: 30 });
        store.dispatch({
            type: "UI_SETUP",
            settings: {
                showDbgCanvas: true
            }
        });
    }

    initPhysics(ammo) {
        this.Ammo = ammo;
        this.ammoHeightData = this.Ammo._malloc(4 * 256 * 256);
    }

    createPhysicsShape() {
        // This parameter is not really used, since we are using PHY_FLOAT height data type and hence it is ignored
        var heightScale = 1;
        // Up axis = 0 for X, 1 for Y, 2 for Z. Normally 1 = Y is used.
        var upAxis = 1;
        // hdt, height data type. "PHY_FLOAT" is used. Possible values are "PHY_FLOAT", "PHY_UCHAR", "PHY_SHORT"
        var hdt = "PHY_FLOAT";
        // Set this to your needs (inverts the triangles)
        var flipQuadEdges = false;

        // Creates the heightfield physics shape
        this.heightFieldShape = new this.Ammo.btHeightfieldTerrainShape(
            this.terrainWidth,
            this.terrainHeigth,
            this.ammoHeightData,
            heightScale,
            0,
            this.heigthScale + 50,
            upAxis,
            hdt,
            flipQuadEdges
        );
        this.heightFieldShape.setLocalScaling(new this.Ammo.btVector3(this.sizeScale, 1, this.sizeScale));
        this.heightFieldShape.setMargin(0.05);
    }

    // batch size: 256 * 64 = 16384
    updateGeometryProgressive = (time = 0) => {

        // console.log("time: " + time);

        var chunkSize = (this.terrainWidth * this.terrainHeigth) / this.terrainGeom.userData.batchCount;
        const vertMin = this.terrainGeom.userData.currentBatch * chunkSize;
        const vertMax = (this.terrainGeom.userData.currentBatch + 1) * chunkSize;

        // var getHeight = (x, y, t) => this.heigthFunc(x, y, Math.round(t));

        var vertices = this.terrainGeom.attributes.position.array;
        var uvs = this.terrainGeom.attributes.uv.array;

        var i2 = 0;

        for (var i = vertMin; i < vertMax; i++) {
            var row = i % (this.terrainWidth);
            var col = Math.floor(i / (this.terrainWidth));
            var heigth = this.getHeight(row, col, time);
            vertices[i * 3 + 2] = heigth * this.heigthScale;


            // fill physics shape array
            if (this.ammoHeightData) {
                this.Ammo.HEAPF32[this.ammoHeightData + i2 >> 2] = heigth * this.heigthScale + 50;
                i2 += 4;
            }
            // fill heigth array
            this.heightsArray[i] = heigth;
        }

        if (vertMax == (vertices.length / 3)) {
            // console.log("DONE updateGeometry by batch ");
            this.terrainGeom.computeVertexNormals();
            this.terrainGeom.attributes.position.needsUpdate = true;
            this.terrainGeom.userData.currentBatch = 0;
        } else {
            this.terrainGeom.userData.currentBatch++;
        }
    }

    update = (time) => {
        // var T = Math.round(time);
        var Tcos = Math.cos(time);
        this.updateGeometryProgressive((Tcos + 1) / 2);
    }

    //@todo test what x and y means for canvas, 
    /**
     * y
     * ^
     * |
     * |____> x
     */
    getHeight = (x, y, t) => {
        //    return fn? fn(x,y): 0;
        return this.heigthFunc(x, y, t);
    }

    fillCanvasData(canvas, t) {
        // var canvas = document.getElementById("testcanvas");
        var ctx = canvas.getContext("2d");
        // read the width and height of the canvas
        // draw white background
        ctx.fillStyle = "#FFFFFF";
        ctx.fillRect(0, 0, 128, 128);
        ctx.beginPath();
        ctx.arc(95, 50, 40, 0, 2 * Math.PI);
        ctx.stroke();

        function setPixel(imageData, x, y, c, a) {
            var index = (x + y * imageData.width) * 4;
            imageData.data[index + 0] = c; //r
            imageData.data[index + 1] = c; //g
            imageData.data[index + 2] = c; //b
            imageData.data[index + 3] = a;
        }

        // create a new pixel array
        var imageData = ctx.createImageData(canvas.width, canvas.height);

        //TODO: fill image data from height array instead
        let p = 0;
        for (let x = 0; x < canvas.width; x++) {
            for (let y = 0; y < canvas.height; y++) {
                let color = this.getHeight(x, y) * 256;
                let flag = ((x % 5) % 4) * ((y % 5) % 4) * 255;
                flag = (x + y) % t;
                // flag = 0.5;
                setPixel(imageData, x, y, color, flag * 255); // 255 opaque
                p++;
            }
        }

        // copy the image data back onto the canvas
        ctx.putImageData(imageData, 0, 0); // at coords 0,0
    }

}


export { HeightFieldTerrain};