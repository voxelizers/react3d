import * as THREE from 'three/build/three.module';
import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise";
import {HeightFieldTerrain} from './HeightField'

var LandscapePresets = {};
var HeigthFuncCatalog = {};
var HeightModifiers = {};

LandscapePresets.noiseGround = (terrainSize) => {
    var simplex = new SimplexNoise()

    var heightfieldTerrain = new HeightFieldTerrain(terrainSize, (x,y,t) => HeigthFuncCatalog.fnNoise(x,y,t,simplex));

    var terrainGeom = new THREE.PlaneBufferGeometry(1024, 1024, terrainSize - 1, terrainSize - 1);
    //geometry.rotateX(-Math.PI / 2);
    heightfieldTerrain.updateGeometryProgressive(terrainGeom, 0);
    
    terrainGeom.userData.heightFieldHelper = heightfieldTerrain;
    // terrainSize = 128:   chunks = 1 => batchSize = 128*128
    //                      chunks = 4 => batchSize = 128*128/4
    terrainGeom.userData.update = (time) => heightfieldTerrain.updateGeometryProgressive(terrainGeom, time/10);
    return terrainGeom;
}

LandscapePresets.noiseSmooth = (terrainSize) => {
    var simplex = new SimplexNoise()

    var heightfieldTerrain = new HeightFieldTerrain(terrainSize, (x,y,t) => HeigthFuncCatalog.fnNoiseSmoothEdges(x,y,t,simplex));

    var terrainGeom = new THREE.PlaneBufferGeometry(1024, 1024, terrainSize - 1, terrainSize - 1);
    //geometry.rotateX(-Math.PI / 2);
    heightfieldTerrain.updateGeometryProgressive(terrainGeom, 0);
    
    terrainGeom.userData.heightFieldHelper = heightfieldTerrain;
    // terrainSize = 128:   chunks = 1 => batchSize = 128*128
    //                      chunks = 4 => batchSize = 128*128/4
    terrainGeom.userData.update = (time) => heightfieldTerrain.updateGeometryProgressive(terrainGeom, time);
    return terrainGeom;
}

LandscapePresets.monticule = () => {

}

LandscapePresets.vallon_rock = (terrainSize) => {
    var geom = LandscapePresets.noiseGround(terrainSize);
    geom.userData.heightFieldHelper.applyGeometryModifier(geom,(x,y,h)=>HeightModifiers.vallon_rock(x,y,h));
    return geom;
};

LandscapePresets.vallon_sand = (terrainSize) => {
    var geom = LandscapePresets.noiseSmooth(terrainSize);
    geom.userData.heightFieldHelper.applyGeometryModifier(geom,(x,y,h)=>HeightModifiers.vallon_sand(x,y,h));
    return geom;
};

LandscapePresets.surrounding_mountains = (terrainSize) => {
    var geom = LandscapePresets.noiseGround(terrainSize);
    geom.userData.heightFieldHelper.applyGeometryModifier(geom,(pt)=>HeightModifiers.surrounding_mountains(pt, terrainSize));
    return geom;
};

HeigthFuncCatalog.fn1 = (x, y) => {
    var height = (Math.sin(y / 8) + 1) / 2 * (Math.sin(x / 4) + 1) / 2;
    var vect = new THREE.Vector2(x - this.terrainSize / 2, y - this.terrainSize / 2);
    var dist = vect.length();
    height = Math.sin(dist / 10);
    // height = getNoise(x,y);
    return height;
}

HeigthFuncCatalog.fn2 = (x, y) => {
    if (x > 128 && y > 128) {
        return 1;
    } else {
        return 0;
    }
}

HeigthFuncCatalog.fnNoise = (x, y, t=0, simplex) => {
    var freq = [0.0125, 0.025, 0.05, 0.1, 0.2, 0.4, 0.8];
    var noise = 0;
    for (var i = 0; i < freq.length; i++) {
        noise += (simplex.noise3d(x * freq[i], y * freq[i], t) + 1) / Math.pow(2, i + 1);//
    }
    // noise = Math.sin(2*Math.PI*x/256);
    return noise;
}

HeigthFuncCatalog.fnNoiseMonticule = (x, y, t) => {
    // if(x == 0 || y==0 || x == (this.terrainSize-1) || y == (this.terrainSize-1)) return 0;
    // else {
    var current = new THREE.Vector2(x, y); 
    var middle = new THREE.Vector2(this.terrainSize/2, this.terrainSize/2);
    var vectDiff = middle.clone().negate();
    vectDiff.add(current);
    var diff = this.terrainSize/2 - vectDiff.length();
    diff = (diff > 0)? diff/this.terrainSize: -0.01;
    
    return diff * this.fnNoise(x,y) * 2;
    // }
}

HeigthFuncCatalog.fnNoiseSmoothEdges = (x, y, t, simplex) => {
    var coords = new THREE.Vector2(x, y);
    var coordsMax = new THREE.Vector2(128, 128);
    var coef = HeightModifiers.smoothEdges(coords, coordsMax);
    return (HeigthFuncCatalog.fnNoise(x,y,t,simplex)-0.8)*coef - 0.01;
    // return HeigthFuncCatalog.fnNoise(x,y,t,simplex);
    // }
}

HeightModifiers.smoothEdges = (coord, coordMax) => {
    // if(x == 0 || y==0 || x == (this.terrainSize-1) || y == (this.terrainSize-1)) return 0;
    // else {
    var terrainSize = (coordMax.x-1);
    var middle = coordMax.multiplyScalar(0.5);
    var vectDiff = middle.clone().negate();
    vectDiff.add(coord);
    var diff = (terrainSize/2 - vectDiff.length())/terrainSize;
    var coef = (diff > 0)? diff: -0.01;
    
    // if (diff <= 0.1)
    // return coef * this.fnNoise(x,y);
    // else
    coef = 1;
    if(coord.x <= terrainSize*0.1)
        coef *= coord.x/(terrainSize*0.1)
    if(coord.y <= terrainSize*0.1)
        coef *= coord.y/(terrainSize*0.1)
    if(coord.x >= terrainSize*0.9)
        coef *= (1 - coord.x/terrainSize)/0.1
    if(coord.y >= terrainSize*0.9)
        coef *= (1 - coord.y/terrainSize)/0.1
    return coef;
    // }
}

HeightModifiers.vallon_rock = (x,y,h) => {
    // return h*2*Math.sin(x)
    var sizeFactor = 2;
    // var newVal = 128*Math.sin((x+28)/20)+64;
    var newVal = 128*sizeFactor*Math.sin((x+28*sizeFactor)/(20*sizeFactor))+64;
    // newVal = newVal>-10? 0.5*newVal + 2*h/sizeFactor: newVal+ 2*h;
    // newVal = newVal>0? 2.5*h+1.5*h*newVal+64*newVal:newVal*64+1.5*h;
    newVal = newVal>-10? 0.4*newVal + 2.5*h: newVal+ 2*h;
    return newVal;
}

HeightModifiers.surrounding_mountains = (point, ts) => {
    // var pos = new THREE.Vector2()
    var pt = new THREE.Vector2(point.x-ts/2, point.y-ts/2);
    var h = point.z;
    var l = pt.length();
    var d = l-ts/2+ts/20;
    if(l > ts/2){
        return Math.abs(h*(1+1/2+10*(d-ts/20)/ts));
    } else
    if(d > 0){
        return Math.abs(h*(1+d*10/ts));
    } else
    return h/2;
}

HeightModifiers.vallon_sand = (x,y,h) => {
    // return h*2*Math.sin(x)
    var newVal = 128*Math.sin((x+28)/20)+64;
    newVal = newVal>-50? 0.5*(newVal + h): newVal+ h;
    // newVal = newVal>0? 2.5*h+1.5*h*newVal+64*newVal:newVal*64+1.5*h;
    return newVal;
}

export  {HeightModifiers, HeigthFuncCatalog, LandscapePresets};