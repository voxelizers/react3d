import { combineReducers } from 'redux'
import { createReducer } from '@reduxjs/toolkit'

const UiSettings = createReducer({
    showDbgCanvas: false,
    showTimeline: false,
    showGraph: false
}, {
    UI_SETUP: (state, action) => {
        state = action.payload.settings;
    }
});

/*
updateValue(state, action) {
    const {someId, someValue} = action.payload;
    state.first.second[someId].fourth = someValue;
}
*/

const DemoSamples = createReducer({
    time: {
        current: 0,
        custom: {
            hour: 0,
            min: 0
        }
    },
    debug: {
    },
    backup: {
        time: 0
    },
    commands: {
        locking: false,
        trigger: false,
        togglePause: false,
        switch: false,
        left: 0,
        right: false,
        up: false,
        down: false
    }
}, {
    GLOBAL: (state, action) => {
        state.sampleType = action.payload.sampleType;
        state.sampleName = action.payload.sampleName;
        state.sampleId = action.payload.sampleId;
    },
    COMMAND: (state, action) => {
        state.commands.locking = (action.payload.btn === 2 || action.payload.key === 13) && action.payload.isDown;
        state.commands.next = action.payload.btn === 4 && action.payload.isDown;
        state.commands.prev = action.payload.btn === 3 && action.payload.isDown;
        state.commands.trigger = action.payload.btn === 1 && action.payload.isDown;
        state.commands.togglePause = action.payload.key === 32 && action.payload.isDown ? !state.commands.togglePause : state.commands.togglePause;
        state.commands.switch = action.payload.key === 9 && action.payload.isDown ? !state.commands.switch : state.commands.switch;
        state.commands.movex = action.payload.moveX ? action.payload.moveX : 0;
        state.commands.movey = action.payload.moveY ? action.payload.moveY : 0;
        state.commands.left = action.payload.key === 37 && action.payload.isDown ? state.commands.left + 1 : 0;
        state.commands.right = action.payload.key === 39 && action.payload.isDown ? state.commands.right + 1 : 0;
        state.commands.up = action.payload.key === 38 && action.payload.isDown;
        state.commands.down = action.payload.key === 40 && action.payload.isDown;
    },
    TIMING: (state, action) => {
        state.time.current = action.payload.current ? action.payload.current : state.time.current; //"SET_CURRENT"
        state.time.custom = action.payload.custom ? action.payload.custom : state.time.custom; //"SET_CUSTOM"
        // state.time.backup = action.payload.backup ? action.payload.backup : state.time.backup; //"BACKUP"
    },
    BACKUP: (state, action) => {
        var backup = onTheFly(action, state.backup);
        var newState = { backup };
        return Object.assign({}, state, newState)
    },
    DEBUG: (state, action) => {
        var debug = onTheFly(action, state.debug);
        var newState = { debug };
        return Object.assign({}, state, newState)
    },
    MANUAL_TRIGGER: (state, action) => {
        console.log("trigger? " + action.payload.enabled);
        state.commands.trigger = action.payload.enabled
    }
})

// class ENUMS {
    // static 
    const VIS_MODE = { "ENTITIES": 0, "OVERLAP": 1, "RAYCAST": 2 };
    // static 
    const RAYCAST_MODE = { 0: "polygon", 1: "point" };
    // static 
    const GROUP_MODE = { 0: "intersection", 1: "union" };
// }

const Voxels = createReducer({
    mode: null,
    selectedEntity: null,
    entities: {
        transform: {},
        status: {},
        groupMode: 0,
        groups: []
        // matrices: {}
    },
    raycast: {
        locked: false,
        mode: 0, //0:point, 1:polygon, ...
        index: null,
        entityId: null,
        voxelPos: null
    },
    rendering: {
        refresh: false
    }
}, {
    SELECT_ENTITY: (state, action) => {
        state.selectedEntity = action.payload,
            state.raycast.entityId = action.payload
    },
    ADD_ENTITY: (state, action) => {
        var id = action.payload.entityId;
        state.entities.status[id] = {};
        state.entities.status[id][id] = null;
    },
    // update matrices
    MOVE_ENTITY: (state, action) => {
        var id = action.payload.entityId;
        var mat = action.payload.transform;
        var newmat = state.entities.transform[id] ? state.entities.transform[id].clone().multiply(mat) : mat;
        state.entities.transform[id] = newmat;
    },
    CREATE_GROUP: (state, action) => {
        var entityId1 = action.payload.entities[0];
        var entityId2 = action.payload.entities[1];
        var found1, found2;
        var groups = state.entities.groups;
        var index = groups.findIndex(function (grp) {
            var found = grp.findIndex(function (id) {
                found1 = id === entityId1;
                found2 = id === entityId2;
                return found1 || found2;
            });
            return found !== -1;
        });
        if (index !== -1) {
            if (!found1) groups[index].push(entityId1); else if (!found2) groups[index].push(entityId2);
        } else {
            groups.push([entityId1, entityId2]);
        };
        state.entities.groups = groups;
    },
    ENTITY_OVERLAP_UPDATE: (state, action) => {
        // var entity1 = action.payload.entity1;
        // var entity2 = action.payload.entity2;
        // var val = action.payload.box;
        // state.entities.overlapStatus[entity1][entity2] = val;
        // state.entities.overlapStatus[entity2][entity1] = val;
        var id1 = action.payload.entityId;
        action.payload.overlapBoxes.forEach((val, id2) => {
            var oldVal = state.entities.status[id1][id2];
            if (oldVal || val) {    // if at least one val is not null
                state.entities.status[id1][id2] = val;
                state.entities.status[id2][id1] = val;
            }
        });
    },
    RAYCAST_SET: (state, action) => {
        state.raycast.index = action.payload.index;
        state.raycast.entityId = action.payload.entityId;
        state.raycast.voxelPos = action.payload.voxelPos;
    },
    RAYCAST_TOGGLE_LOCK: (state, action) => {
        state.raycast.locked = !state.raycast.locked && (state.raycast.index !== null || state.selectedEntity !== null);
    },
    RAYCAST_SWITCH_MODE: (state, action) => {
        var modesCount = Object.keys(RAYCAST_MODE).length;
        state.raycast.mode = (state.raycast.mode + 1) % modesCount;
    },
    RAYCAST_UNSET: (state, action) => {
        state.raycast.index = null;
    },
    VOXEL_INSTANCES_REFRESHED: (state, action) => {
        state.rendering.refresh = action.payload.active
    },
    VIS_MODE: (state, action) => {
        var modeCount = Object.keys(VIS_MODE).length;
        state.mode = action.payload ? action.payload.mode : (state.mode + 1) % modeCount;
    }
})

const rootReducer = combineReducers({
    DemoSamples, UiSettings, Voxels
})

export default rootReducer

function onTheFly(action, current) {
    var next = Object.assign({}, current);
    switch (action.subtype) {
        case "STORE_VAL":
            next[action.key] = action.val
            // console.log(current.debug);
            break;
        case "ACC_VAL":
            var prevVal = next[action.key] ? next[action.key] : 0;
            next[action.key] = prevVal + action.val;
            // console.log(current.debug);
            break;
        case "ACC_ARR":
            var prevVal = next[action.key] ? next[action.key] : [];
            next[action.key] = [...prevVal, action.val];
            // console.log(current.debug);
            break;
        case "INC_VAL":
            next[action.key] = next[action.key] + 1;
            break;
    }
    return next
}

export { RAYCAST_MODE, VIS_MODE };
