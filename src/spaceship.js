import * as THREE from 'three';
import * as TOOLS from './Toolbox';
// import 'three/examples/js/utils/SceneUtils.js';



export class Spaceship {

    THREE; Ammo;
    context;
    scene;
    instances;
    isLoaded; isInitDone;
    currentWaypoint = 0;

    constructor(context) {
        this.THREE = THREE;
        this.Ammo = window.Ammo;
        this.scene = context.scene;
        this.context = context;
        this.loadModel();
        this.MISC_TOOLS = new TOOLS.Misc();
        this.instances = [];
    }

    loadModel() {
        var onProgress = function (xhr) {
            if (xhr.lengthComputable) {
                var percentComplete = xhr.loaded / xhr.total * 100;
                console.log(Math.round(percentComplete, 2) + '% downloaded');
            }
        };
        var onError = function (xhr) { };

        // THREE.Loader.Handlers.add( /\.dds$/i, new THREE.DDSLoader() );

        new this.THREE.MTLLoader()
            .setPath('/')
            .load('TIE-fighter.mtl', (materials) => {
                materials.preload();
                new this.THREE.OBJLoader()
                    .setMaterials(materials)
                    .setPath('/')
                    .load('TIE-fighter2.obj', (object) => {
                        var scale = 0.35;
                        object.scale.x = scale;
                        object.scale.y = scale;
                        object.scale.z = scale;
                        object.name = "spaceship";
                        // this.scene.add(object);
                        this.spaceshipModel = object;
                        this.isLoaded = true;
                    }, onProgress, onError);
            });
    }

    initSpaceship() {
        this.spaceshipModel.receiveShadow = true;
        this.spaceshipModel.castShadow = true;
        this.spaceshipModel.traverse((child) => {

            if (child instanceof THREE.Mesh) {
                // console.log(child);
                child.receiveShadow = true;
                child.castShadow = true;
            }

        });

        
        // INSTANCES
        // spaceship 1
        var spaceship1 = this.spaceshipModel.clone();
        spaceship1.name = "spaceship1";
        spaceship1.position.set(0, 20, 0);
        this.scene.add(spaceship1);
        spaceship1.userData.speed = 0;
        spaceship1.userData.type = 0;
        spaceship1.userData.state = 0;
        this.createBody(spaceship1);

        // spaceship 2
        var spaceship2 = this.spaceshipModel.clone();
        spaceship2.name = "spaceship2";
        spaceship2.position.set(50, 50, 50);
        this.scene.add(spaceship2);
        spaceship2.userData.speed = 0;
        spaceship2.userData.type = 1;
        spaceship2.userData.state = 0;

        // spaceship 3
        var spaceship3 = this.spaceshipModel.clone();
        spaceship3.name = "spaceship3";
        spaceship3.position.set(-50, 100, -50);
        this.scene.add(spaceship3);
        spaceship3.userData.type = 1;
        spaceship3.userData.speed = 0;
        spaceship3.userData.type = 2;
        spaceship3.userData.state = 0;
        this.createBody(spaceship3);


        this.isInitDone = true;
    }

    createBody(spaceship) {
        // var bbox = new THREE.Box3().setFromObject(spaceship);
        // var helper = new THREE.Box3Helper(bbox, 0xffff00);
        // this.scene.add(helper);
        // var shape = this.MISC_TOOLS.createConvexHullPhysicsShape(helper.geometry.attributes.position.array, this.Ammo);

        var shape = new this.Ammo.btBoxShape(new this.Ammo.btVector3(18, 18, 10));

        var margin = 0.05; var mass = 30;
        shape.setMargin(margin);
        var localInertia = new this.Ammo.btVector3(0, 0, 0);
        shape.calculateLocalInertia(mass, localInertia);
        var transform = new this.Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new this.Ammo.btVector3(spaceship.position.x, spaceship.position.y, spaceship.position.z));
        var motionState = new this.Ammo.btDefaultMotionState(transform);
        var rbInfo = new this.Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
        var body = new this.Ammo.btRigidBody(rbInfo);
        spaceship.userData.physicsBody = body;
        spaceship.userData.physicsCb = () => { };
        this.context.dynamicObjects.push(spaceship);
        this.context.physicsWorld.addRigidBody(body);
    }

    followRoute(id) {
        var spaceship = this.scene.getObjectByName("spaceship"+id);
        if (spaceship.userData.state) {
            var spaceshipPath = this.scene.getObjectByName("spaceshipPath");
            var spaceshipBody = spaceship.userData.physicsBody;
            var speed = spaceship.userData.speed * 30;
            var p = spaceshipBody.getWorldTransform().getOrigin();


            var posAttrArray = spaceshipPath.geometry.attributes.position.array;
            var targetPos = new this.THREE.Vector3(posAttrArray[this.currentWaypoint * 3], posAttrArray[this.currentWaypoint * 3 + 1], posAttrArray[this.currentWaypoint * 3 + 2]);
            var originPos = this.MISC_TOOLS.convertThreeBulletVect(p);
            var diffVect = targetPos.add(originPos.negate());

            if ((speed && diffVect.length() <= 5)) {
                var maxWaypoint = (posAttrArray.length / 3) - 1;
                this.currentWaypoint = ((this.currentWaypoint + 1) % maxWaypoint);
                console.log("position reached! going to next waypoint: " + this.currentWaypoint);

                targetPos = new this.THREE.Vector3(posAttrArray[this.currentWaypoint * 3], posAttrArray[this.currentWaypoint * 3 + 1], posAttrArray[this.currentWaypoint * 3 + 2]);
                originPos = this.MISC_TOOLS.convertThreeBulletVect(p);
                diffVect = targetPos.add(originPos.negate());

                var quatRot = new this.THREE.Quaternion(0, 0, 0, 1);
                quatRot.setFromUnitVectors(new this.THREE.Vector3(0, 0, 1), diffVect.normalize());

                spaceshipBody.getWorldTransform().setRotation(new this.Ammo.btQuaternion(quatRot.x, quatRot.y, quatRot.z, quatRot.w));
            }

            if (spaceship.userData.state == 1) {
                // console.log(rotVect);
                // spaceshipBody.applyTorqueImpulse(new this.Ammo.btVector3(rotVect.x * torque, rotVect.y * torque, 0));
                var quatRot = new this.THREE.Quaternion(0, 0, 0, 0);
                quatRot.setFromUnitVectors(new this.THREE.Vector3(0, 0, 1), diffVect.normalize());
                var wt = spaceshipBody.getWorldTransform().setRotation(new this.Ammo.btQuaternion(quatRot.x, quatRot.y, quatRot.z, quatRot.w));
                spaceship.userData.speed = 1;
                // spaceship.userData.state++;
            }

            this.updateVelocity(id)
        }
    }

    followTarget(id, targetPos, speed) {
        this.scene.getObjectByName("spaceship"+id).lookAt(targetPos.x, targetPos.y, targetPos.z);
    }

    fireLaser(targetPos, id) {
        var spaceship = this.scene.getObjectByName("spaceship"+id);
        var srcPos = spaceship.position.clone();
        srcPos.x += 2;
        srcPos.z += 10;
        var srcQuat = spaceship.quaternion;

        // Laser beam
        var geometry = new THREE.CylinderGeometry(1, 1, 5, 16);
        geometry.rotateX(-Math.PI / 2);
        var material = new THREE.MeshBasicMaterial({ color: 0xff0000 });
        var threeObject = new THREE.Mesh(geometry, material);
        threeObject.position.set(srcPos.x, srcPos.y, srcPos.z);
        threeObject.name = "laser"
        this.scene.add(threeObject);

        var mass = 1; var margin = 0.05;
        var shape = new this.Ammo.btCapsuleShape(1, 5);
        shape.setMargin(margin);

        var localInertia = new this.Ammo.btVector3(0, 0, 0);
        shape.calculateLocalInertia(mass, localInertia);
        var transform = new this.Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new this.Ammo.btVector3(srcPos.x, srcPos.y, srcPos.z));
        transform.setRotation(new this.Ammo.btQuaternion(srcQuat.x, srcQuat.y, srcQuat.z, srcQuat.w));
        var motionState = new this.Ammo.btDefaultMotionState(transform);
        var rbInfo = new this.Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
        var body = new this.Ammo.btRigidBody(rbInfo);
        threeObject.userData.physicsBody = body;
        threeObject.userData.physicsCb = () => { };
        threeObject.userData.speed = 0;
        this.scene.add(threeObject);
        this.context.dynamicObjects.push(threeObject);
        this.context.physicsWorld.addRigidBody(body);

        // trigger
        var dirVect = new this.THREE.Vector3(0, 0, 1000);
        dirVect.applyQuaternion(srcQuat);
        body.setLinearVelocity(new this.Ammo.btVector3(dirVect.x, dirVect.y, dirVect.z));
        body.setGravity(0);
        body.setActivationState(true);
    }

    pointingLaser(id) {
        var spaceship = this.scene.getObjectByName(id);
    }

    animLaserBeam() {
        var threeObj = this.scene.getObjectByName("laser");
        var threeObj2 = this.scene.getObjectByName("spaceship2");
        var ammoObj = threeObj.userData.physicsBody;

        var speed = threeObj.userData.speed;

        var p = ammoObj.getWorldTransform().getOrigin();
        var q = ammoObj.getWorldTransform().getRotation();
        var quat = new this.THREE.Quaternion(q.x(), q.y(), q.z(), q.w());


        var quatRot = threeObj2.quaternion;
        var wt = ammoObj.getWorldTransform().setRotation(new this.Ammo.btQuaternion(quatRot.x, quatRot.y, quatRot.z, quatRot.w));

        var dirVect = new this.THREE.Vector3(0, 0, 1);
        dirVect.applyQuaternion(quat);
        threeObj.userData.physicsBody.setLinearVelocity(new this.Ammo.btVector3(dirVect.x * speed, dirVect.y * speed, dirVect.z * speed));
        threeObj.userData.physicsBody.setActivationState(true);
    }

    attachCamTo(id, cam) {
        var spaceship = this.scene.getObjectByName("spaceship"+id);
        // cam.children.push(spaceship);
        // spaceship.children.push(cam);
        // cam.useTarget = true;
        // this.scene.remove( cam );
        spaceship.add( cam );
        cam.position.set(4, 0, 2);// coordinates in local ref
        // this.THREE.SceneUtils2.attach(cam, this.scene, spaceship);
    }

    updateVelocity(id) {
        var spaceship = this.scene.getObjectByName("spaceship"+id);
        var spaceshipBody = spaceship.userData.physicsBody;
        var speed = spaceship.userData.speed * 30;
        var q = spaceshipBody.getWorldTransform().getRotation();
        var quat = new this.THREE.Quaternion(q.x(), q.y(), q.z(), q.w());
        var dirVect = new this.THREE.Vector3(0, 0, 1);
        dirVect.applyQuaternion(quat);
        spaceshipBody.setLinearVelocity(new this.Ammo.btVector3(dirVect.x * speed, dirVect.y * speed, dirVect.z * speed));
        spaceshipBody.setActivationState(true);
    }

    update(id) {
        if (this.isInitDone) {
            var spaceship = this.scene.getObjectByName("spaceship"+id);
            switch (spaceship.userData.type) {
                case 0:
                    this.followRoute(id);
                    break;
                case 1:
                    this.followTarget(id, this.scene.getObjectByName("spaceship1").position, 0);
                    break;
            }
        } else if (this.isLoaded) {
            this.initSpaceship();
        }
    }
}